package com.kreasikode.qta1.adapter

import android.graphics.Typeface
import android.text.Spannable
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.core.text.toSpannable
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataArtikel
import com.kreasikode.qta1.utils.ImageUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_aksespasar.*

class ArtikelAdapter(
    private val onClickItem: (DataArtikel) -> Unit
): RecyclerView.Adapter<ArtikelAdapter.Holder>() {

    private val listArtikel = mutableListOf<DataArtikel>()

    fun setData(list: List<DataArtikel>) {
        listArtikel.clear()
        listArtikel.addAll(list)
        notifyDataSetChanged()
    }

    fun getData(): List<DataArtikel> = listArtikel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_aksespasar, parent, false))
    }

    override fun getItemCount(): Int = listArtikel.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listArtikel[position], onClickItem)
    }


    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(artikelFarm: DataArtikel, onClickItem: (DataArtikel) -> Unit) {
//            ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, artikelFarm.img)!!, iv_pasar)
//            tv_pasar_name.text = artikelFarm.title
//            tv_alamat.text = artikelFarm.description
//            rl_item.setOnClickListener { onClickItem(artikelFarm) }

            if (artikelFarm.thumbnail_img.isNullOrEmpty()) {
                ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context.applicationContext, R.drawable.no_image)!!, iv_pasar)
            }else {
                ImageUtil.fromUrl(containerView.context.applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(artikelFarm.thumbnail_img), iv_pasar)
            }
            tv_pasar_name.text = artikelFarm.title
            val describe = HtmlCompat.fromHtml(when(artikelFarm.excerpt) {
                "<p>null</p>", null -> artikelFarm.content?:"No description avaliable"
                else -> artikelFarm.excerpt
            }, HtmlCompat.FROM_HTML_MODE_LEGACY)
            describe.toSpannable().setSpan(StyleSpan(Typeface.NORMAL), 0, describe.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            tv_alamat.text = describe.toString()
            rl_item.setOnClickListener { onClickItem(artikelFarm) }
        }
    }
}