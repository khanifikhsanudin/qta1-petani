package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.text.Spannable
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.core.text.toSpannable
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataArtikel
import com.kreasikode.qta1.utils.ImageUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_artikel_slider.*

class ArtikelSliderAdapter(
    private val onClickItem: (DataArtikel) -> Unit
): CardSliderAdapter<ArtikelSliderAdapter.Holder>() {

    private val listArtikel = mutableListOf<DataArtikel>()

    fun setData(list: List<DataArtikel>) {
        listArtikel.clear()
        listArtikel.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_artikel_slider, parent, false))
    }

    override fun getItemCount(): Int = listArtikel.size

    override fun bindVH(holder: Holder, position: Int) {
        holder.bindItem(listArtikel[position], onClickItem)
    }

    inner class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        @SuppressLint("SetTextI18n")
        fun bindItem(artikelFarm: DataArtikel, onClickItem: (DataArtikel) -> Unit) {
            if (artikelFarm.thumbnail_img.isNullOrEmpty()) {
                ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, R.drawable.no_image)!!, iv_artikel)
            }else {
                ImageUtil.fromUrl(containerView.context.applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(artikelFarm.thumbnail_img), iv_artikel)
            }
            tv_artikel_title.text = artikelFarm.title
            tv_artikel_author.text = "Oleh: ${artikelFarm.agency}"
            val describe = HtmlCompat.fromHtml(when(artikelFarm.excerpt) {
                "<p>null</p>", null -> artikelFarm.content?:"No description avaliable"
                else -> artikelFarm.excerpt
            }, HtmlCompat.FROM_HTML_MODE_LEGACY)
            describe.toSpannable().setSpan(StyleSpan(Typeface.NORMAL), 0, describe.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            tv_artikel_desc.text = describe.toString()
            rl_item.setOnClickListener { onClickItem(artikelFarm) }
        }
    }
}