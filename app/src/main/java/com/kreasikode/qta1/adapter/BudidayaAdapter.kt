package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.DataBodySpk
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_budidaya.*

class BudidayaAdapter(
    private val onClickDetail: (DataBodySpk) -> Unit
): RecyclerView.Adapter<BudidayaAdapter.Holder>() {

    private val listDataBodySpk = mutableListOf<DataBodySpk>()

    fun setData(list: List<DataBodySpk>) {
        listDataBodySpk.clear()
        listDataBodySpk.addAll(list)
        notifyDataSetChanged()
    }

    fun clearData() {
        listDataBodySpk.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_budidaya, parent, false))
    }

    override fun getItemCount(): Int = listDataBodySpk.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listDataBodySpk[position])
    }

    inner class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        @SuppressLint("SetTextI18n")
        fun bindItem(dataBodySpk: DataBodySpk) {
            tv_number.text = (adapterPosition + 1).toString()
            tv_nomor_reg_budidaya.text = "${dataBodySpk.reg_budidaya} " +
                    "(${dataBodySpk.nomor_kontrak})\n" +
                    Utility.setFormatRupiah(dataBodySpk.nom_investasi_budidaya?.toIntOrNull()?:0)
            btn_detail.setOnClickListener { onClickDetail(dataBodySpk) }
        }
    }
}