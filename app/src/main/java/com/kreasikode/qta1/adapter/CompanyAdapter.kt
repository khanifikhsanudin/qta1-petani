package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataVendorMerchant
import com.kreasikode.qta1.utils.ImageUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_aksespasar.*

class CompanyAdapter(
    private val onClickItem: (DataVendorMerchant) -> Unit
): RecyclerView.Adapter<CompanyAdapter.Holder>() {

    private val listCompanyFarm = mutableListOf<DataVendorMerchant>()

    fun setData(list: List<DataVendorMerchant>) {
        listCompanyFarm.clear()
        listCompanyFarm.addAll(list)
        notifyDataSetChanged()
    }

    fun getData(): List<DataVendorMerchant> = listCompanyFarm

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_aksespasar, parent, false))
    }

    override fun getItemCount(): Int = listCompanyFarm.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listCompanyFarm[position], onClickItem)
    }


    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        @SuppressLint("SetTextI18n", "DefaultLocale")
        fun bindItem(companyFarm: DataVendorMerchant, onClickItem: (DataVendorMerchant) -> Unit) {
            if (companyFarm.foto_path.isNullOrEmpty()) {
                ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, R.drawable.no_image)!!, iv_pasar)
            }else {
                ImageUtil.fromUrl(containerView.context.applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(companyFarm.foto_path), iv_pasar)
            }

            tv_pasar_name.text = companyFarm.vendor
            tv_alamat.text = "${companyFarm.kelurahan}, ${companyFarm.kecamatan} ${companyFarm.kabupaten} ${companyFarm.provinsi}".toLowerCase().capitalize()
            rl_item.setOnClickListener { onClickItem(companyFarm) }
        }
    }
}