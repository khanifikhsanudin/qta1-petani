package com.kreasikode.qta1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.CompanyFarm
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_investor.*

class CompanyHomeAdapter(
    private val onClickInvest: (CompanyFarm) -> Unit
): RecyclerView.Adapter<CompanyHomeAdapter.Holder>() {

    private val listCompanyFarm = mutableListOf<CompanyFarm>()

    fun setData(list: List<CompanyFarm>) {
        listCompanyFarm.clear()
        listCompanyFarm.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_investor, parent, false))
    }

    override fun getItemCount(): Int = listCompanyFarm.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        when(position) {
            0 -> { Utility.setMargins(holder.containerView, 18, 0, 5, 0) }
            listCompanyFarm.lastIndex -> { Utility.setMargins(holder.containerView, 5, 0, 18, 0) }
            else -> { Utility.setMargins(holder.containerView, 5, 0, 5, 0) }
        }
        holder.bindItem(listCompanyFarm[position], onClickInvest)
    }


    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(companyFarm: CompanyFarm, onClickInvest: (CompanyFarm) -> Unit) {
            tv_name.text = companyFarm.investor_name
            tv_address.text = companyFarm.address
            tv_telp.text = companyFarm.telp
            tv_product.text = companyFarm.company_name

            btn_investasi.setOnClickListener { onClickInvest(companyFarm) }
        }
    }
}