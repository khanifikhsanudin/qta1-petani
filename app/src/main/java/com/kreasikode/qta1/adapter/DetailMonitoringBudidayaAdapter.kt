package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.MonitoringBudidaya
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_detail_monitoring_budaya.*

class DetailMonitoringBudidayaAdapter(
    private val onClickDetail: () -> Unit
): RecyclerView.Adapter<DetailMonitoringBudidayaAdapter.Holder>() {

    private val listMonitoringBudidaya = mutableListOf<MonitoringBudidaya>()

    fun setData(list: List<MonitoringBudidaya>) {
        listMonitoringBudidaya.clear()
        listMonitoringBudidaya.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_detail_monitoring_budaya, parent, false))
    }

    override fun getItemCount(): Int = listMonitoringBudidaya.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listMonitoringBudidaya[position], position, onClickDetail)
    }


    override fun onViewRecycled(holder: Holder) {
        holder.recycle()
    }

    inner class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        private var btnDetail: Button? = null

        private fun initButtonDetail(): Button {
            if (btnDetail != null) return btnDetail!!
            btnDetail = Button(containerView.context).apply {
                layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                ).apply {
                    gravity = Gravity.END
                    val sizeMargin = containerView.resources.getDimension(R.dimen._14sdp).toInt()
                    val sizeMarginBottom = containerView.resources.getDimension(R.dimen._20sdp).toInt()
                    setMargins(sizeMargin, 0, sizeMargin, sizeMarginBottom)
                    height = containerView.resources.getDimension(R.dimen._24sdp).toInt()
                    setPadding(0)
                }
                setTextColor(ContextCompat.getColor(context, R.color.white))
                setBackgroundResource(R.drawable.bg_corner)
            }
            (containerView as LinearLayout).addView(btnDetail)
            return btnDetail!!
        }


        @SuppressLint("SetTextI18n")
        fun bindItem(monitoringBudidaya: MonitoringBudidaya, position: Int, onClickDetail: () -> Unit) {
            tv_nomor_reg_budidaya.text = monitoringBudidaya.nomor_registrasi_budidaya
            tv_nomor.text = monitoringBudidaya.budidaya_id
            monitoringBudidaya.nominal?.let {
                tv_nominal.text = Utility.setFormatRupiah(it.toInt())
            }

            if (position == (listMonitoringBudidaya.size - 1)) {
                initButtonDetail().apply {
                    text = "Detail"
                    setOnClickListener { onClickDetail() }
                }
            }else {}
        }

        fun recycle() {
            btnDetail?.let { (containerView as LinearLayout).removeView(btnDetail) }
        }
    }
}