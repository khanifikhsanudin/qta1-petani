package com.kreasikode.qta1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataBudidayaBody
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_laporan_keuangan_proses.*

class LaporanKeuanganProsesAdapter(
    private val onClickImage: (DataBudidayaBody, ImageView) -> Unit
): RecyclerView.Adapter<LaporanKeuanganProsesAdapter.Holder>() {

    private val listDataBudidayaBody = mutableListOf<DataBudidayaBody>()

    fun setData(list: List<DataBudidayaBody>) {
        listDataBudidayaBody.clear()
        listDataBudidayaBody.addAll(list)
        notifyDataSetChanged()
    }

    fun clearData() {
        listDataBudidayaBody.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_laporan_keuangan_proses, parent, false))
    }

    override fun getItemCount(): Int  = listDataBudidayaBody.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (position == 0) {
            holder.containerView.findViewById<View>(R.id.line).visibility = View.GONE
        }else {
            holder.containerView.findViewById<View>(R.id.line).visibility = View.VISIBLE
        }
        holder.bindItem(listDataBudidayaBody[position])
    }

    inner class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(dataBudidayaBody: DataBudidayaBody) {

            if (!dataBudidayaBody.gambar_1.isNullOrEmpty()) {
                ImageUtil.fromUrl(containerView.context,
                    EndPoints.BASE_IMAGE_BACK.plus(dataBudidayaBody.gambar_1), iv_process)
            }else {
                ImageUtil.fromDrawable(containerView.context,
                    ContextCompat.getDrawable(containerView.context, R.drawable.no_image)!!, iv_process)
            }

            dataBudidayaBody.tanggal?.let {
                tv_tanggal.text = Utility.indoDateFormat(it, false)
            }

            tv_keterangan.text = dataBudidayaBody.proses
            tv_nominal.text = Utility.setFormatRupiah(dataBudidayaBody.nominal?.toInt()?:0)
            iv_process.setOnClickListener { onClickImage(dataBudidayaBody, iv_process) }
        }
    }
}