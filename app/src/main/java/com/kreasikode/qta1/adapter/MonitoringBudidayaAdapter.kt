package com.kreasikode.qta1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.ui.monitoring.budidaya.ParentMonitoringBudidaya
import com.kreasikode.qta1.utils.Utility
import com.skydoves.expandablelayout.ExpandableLayout
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.collapse_mbudaya.view.*
import kotlinx.android.synthetic.main.expand_mbudaya.view.*
import kotlinx.android.synthetic.main.item_monitoring_budidaya.*

class MonitoringBudidayaAdapter(
    private val gotoDetail: (ParentMonitoringBudidaya) -> Unit
): RecyclerView.Adapter<MonitoringBudidayaAdapter.Holder>() {

    private val listParentMonitoringBudidaya = mutableListOf<ParentMonitoringBudidaya>()

    fun setData(list: List<ParentMonitoringBudidaya>) {
        listParentMonitoringBudidaya.clear()
        listParentMonitoringBudidaya.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_monitoring_budidaya, parent, false))
    }

    override fun getItemCount(): Int = listParentMonitoringBudidaya.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (position % 2 == 0) {
            holder.containerView.findViewById<ExpandableLayout>(R.id.expandable).parentLayout.setBackgroundResource(R.color.white)
            holder.containerView.findViewById<ExpandableLayout>(R.id.expandable).secondLayout.setBackgroundResource(R.color.white)
        }else {
            holder.containerView.findViewById<ExpandableLayout>(R.id.expandable).parentLayout.setBackgroundResource(R.color.primary_trans)
            holder.containerView.findViewById<ExpandableLayout>(R.id.expandable).secondLayout.setBackgroundResource(R.color.primary_trans)
        }
        holder.bindItem(listParentMonitoringBudidaya[position], gotoDetail)
    }

    inner class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(parentMonitoringBudidaya: ParentMonitoringBudidaya, gotoDetail: (ParentMonitoringBudidaya) -> Unit) {
            expandable.parentLayout.tv_nomor_spk.text = parentMonitoringBudidaya.nomor_spk
            expandable.parentLayout.tv_total_nominal.text = Utility.setFormatRupiah(parentMonitoringBudidaya.total_nominal?:0)
            expandable.parentLayout.setOnClickListener{
                if (expandable.isExpanded) {
                    expandable.collapse()
                }else {
                    expandable.expand()
                }
            }

            val detailMonitoringBudidayaAdapter = DetailMonitoringBudidayaAdapter {
                gotoDetail(parentMonitoringBudidaya)
            }
            expandable.secondLayout.rv_detail_monitoring_budaya.layoutManager = LinearLayoutManager(containerView.context)
            expandable.secondLayout.rv_detail_monitoring_budaya.adapter = detailMonitoringBudidayaAdapter
            detailMonitoringBudidayaAdapter.setData(parentMonitoringBudidaya.list)
        }
    }
}