package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.model.DataVendorMerchant
import com.kreasikode.qta1.utils.ImageUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_aksespasar.*
import kotlinx.android.synthetic.main.item_detail_order.*

class OrderAdapter(): RecyclerView.Adapter<OrderAdapter.Holder>() {

    private val listOrder = mutableListOf<DataProduk>()

    fun setData(list: List<DataProduk>) {
        listOrder.clear()
        listOrder.addAll(list)
        notifyDataSetChanged()
    }

    fun getData(): List<DataProduk> = listOrder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_detail_order, parent, false))
    }

    override fun getItemCount(): Int = listOrder.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listOrder[position])
    }


    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        @SuppressLint("SetTextI18n", "DefaultLocale")
        fun bindItem(order: DataProduk) {
            tv_no_detail_order.text = adapterPosition.plus(1).toString()
            tv_name_product_detail.text = order.nama_produk
            tv_qty_detail.text = order.qty.toString()
            val harga = order.qty!!.toInt() * order.harga_ecer!!.toInt()
            tv_harga_detail_order.text = harga.toString().trim()

        }
    }
}