package com.kreasikode.qta1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.ArtikelFarm
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_artikel_lainnya.*

class OtherArtikelAdapter(
    private val onClickItem: (ArtikelFarm) -> Unit
): RecyclerView.Adapter<OtherArtikelAdapter.Holder>() {

    private val listArtikel = mutableListOf<ArtikelFarm>()

    fun setData(list: List<ArtikelFarm>) {
        listArtikel.clear()
        listArtikel.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_artikel_lainnya, parent, false))
    }

    override fun getItemCount(): Int = listArtikel.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        when(position) {
            0 -> { Utility.setMargins(holder.containerView, 18, 0, 5, 0) }
            listArtikel.lastIndex -> { Utility.setMargins(holder.containerView, 5, 0, 18, 0) }
            else -> { Utility.setMargins(holder.containerView, 5, 0, 5, 0) }
        }
        holder.bindItem(listArtikel[position], onClickItem)
    }


    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(artikelFarm: ArtikelFarm, onClickItem: (ArtikelFarm) -> Unit) {
            ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, artikelFarm.img)!!, iv_artikel)
            tv_title.text = artikelFarm.title
            tv_deskripsi.text = artikelFarm.description
            tv_recent.text = artikelFarm.time_recent
            containerView.setOnClickListener { onClickItem(artikelFarm) }
        }
    }
}