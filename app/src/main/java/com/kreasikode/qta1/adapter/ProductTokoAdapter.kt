package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.DataConst
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.model.ItemOrder
import com.kreasikode.qta1.model.ProductToko
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_toko.*
import kotlinx.android.synthetic.main.item_produk.*
import java.util.ArrayList

class ProductTokoAdapter(
    private val onClickItem: (DataProduk) -> Unit
): RecyclerView.Adapter<ProductTokoAdapter.Holder>() {

    private val listProductToko = mutableListOf<DataProduk>()

    fun setData(list: List<DataProduk>) {
        listProductToko.clear()
        listProductToko.addAll(list)
        notifyDataSetChanged()
    }

    fun getData(): List<DataProduk> = listProductToko

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_produk, parent, false))
    }

    override fun getItemCount(): Int = listProductToko.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listProductToko[position], onClickItem)
    }


    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        @SuppressLint("SetTextI18n")
        fun bindItem(productToko: DataProduk, onClickItem: (DataProduk) -> Unit) {
            tv_title.text = "Petani Mitra\nQTA1"
            tv_product_name.text = productToko.nama_produk
            if (productToko.file_path.isNullOrEmpty()) {
                ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, R.drawable.no_image)!!, iv_product)
            }else {
                ImageUtil.fromUrl(containerView.context.applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(productToko.file_path), iv_product)
            }
            tv_price.text = Utility.setFormatRupiah(productToko.harga_ecer!!.toInt())
            tv_satuan.text = "/${productToko.satuan}".replace("gram", "gr")
            ll_item.setOnClickListener { onClickItem(productToko) }
            item_add_bag.setOnClickListener{

                if (DataConst.DataOrder.isEmpty()){
                    val item:DataProduk = productToko
                    item.qty = 1
                    DataConst.DataOrder.add(item)
                }else{
                    val findItem = DataConst.DataOrder.find { it-> productToko.id.equals(it.id) }
                    if (findItem?.id.isNullOrEmpty()){
                        val item:DataProduk = productToko
                        item.qty = 1
                        DataConst.DataOrder.add(item)
                    }
                }
                Toast.makeText(containerView.context.applicationContext,"Produk masuk kedalam tas",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }
}