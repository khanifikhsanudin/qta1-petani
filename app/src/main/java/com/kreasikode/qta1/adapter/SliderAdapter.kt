package co.komiku.komiku.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.kreasikode.qta1.R
import com.kreasikode.qta1.utils.ImageUtil
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_slider.*


class SliderAdapter : SliderViewAdapter<SliderAdapter.Holder>() {

    private val listImg = mutableListOf<Int>()

    fun setData(list: List<Int>) {
        listImg.clear()
        listImg.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup): Holder {
        val inflate: View = LayoutInflater.from(parent.context).inflate(R.layout.item_slider, parent, false)
        return Holder(inflate)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listImg[position])
    }

    override fun getCount(): Int = listImg.size

    inner class Holder(override val containerView: View) : ViewHolder(containerView), LayoutContainer {
        fun bindItem(img: Int){
            ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, img)!!, iv_slider)
        }
    }
}