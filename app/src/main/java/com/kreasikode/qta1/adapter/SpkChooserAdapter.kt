package com.kreasikode.qta1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_chooser_spk.*

class SpkChooserAdapter(
    private val onCLickItem: (Int) -> Unit
): RecyclerView.Adapter<SpkChooserAdapter.Holder>() {

    private val listString = mutableListOf<String>()

    fun setData(list: List<String>) {
        listString.clear()
        listString.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_chooser_spk, parent, false))
    }

    override fun getItemCount(): Int = listString.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listString[position], position)
    }

    inner class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(string: String, position: Int) {
            tv_nomor_spk.text = string
            tv_nomor_spk.setOnClickListener { onCLickItem(position) }
        }
    }
}