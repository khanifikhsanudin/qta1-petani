package com.kreasikode.qta1.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.DataConst
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_order_product.*

class TasAdapter(
    private val list: List<DataProduk>,
    private val onClick: OnClick
): RecyclerView.Adapter<TasAdapter.Holder>() {

    private val listProductToko = mutableListOf<DataProduk>()

    fun set(){
        listProductToko.clear()
        listProductToko.addAll(list)
        notifyDataSetChanged()
    }

    fun setData(list: List<DataProduk>) {
        listProductToko.clear()
        listProductToko.addAll(list)
        notifyDataSetChanged()
    }

    fun getData(): List<DataProduk> = listProductToko

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_order_product, parent, false))
    }

    override fun getItemCount(): Int = listProductToko.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(listProductToko[position],onClick)
    }

    fun removeItem(position: Int) {
        listProductToko.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, listProductToko.size)
    }

    fun restoreItem(data: DataProduk, position: Int) {
        listProductToko.add(position, data)
        notifyItemInserted(position)
    }

    fun getTotal():Int {
        var total = 0
        for (item: DataProduk in listProductToko){
                val harga = item.harga_ecer!!.toInt() * item.qty!!.toInt()
                total = total + harga
        }
        return total
    }

    class Holder(override val containerView: View): RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        @SuppressLint("SetTextI18n")
        fun bindItem(productToko: DataProduk, onClick: OnClick) {
            tv_name_product_tas.text = productToko.nama_produk
            if (productToko.file_path.isNullOrEmpty()) {
                ImageUtil.fromDrawable(containerView.context.applicationContext, ContextCompat.getDrawable(containerView.context, R.drawable.no_image)!!, iv_product_tas)
            }else {
                ImageUtil.fromUrl(containerView.context.applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(productToko.file_path), iv_product_tas)
            }
            var harga = productToko.harga_ecer!!.toInt() * productToko.qty!!.toInt()
            tv_harga_produk_tas.text = Utility.setFormatRupiah(harga)
            tv_qty_order_tas.text = productToko.qty.toString()
            btn_minus_qty.setOnClickListener {
                if (productToko.qty == 0){
                    DataConst.DataOrder.remove(productToko)
                    onClick.clickMinus()
                }else{
                    productToko.qty = productToko.qty?.minus(1)
                    tv_qty_order_tas.text = productToko.qty.toString()
                    harga = productToko.harga_ecer!!.toInt() * productToko.qty!!.toInt()
                    tv_harga_produk_tas.text = Utility.setFormatRupiah(harga)
                    onClick.clickMinus()
                }


            }
            btn_plus_qty.setOnClickListener {
                productToko.qty = productToko.qty?.plus(1)
                tv_qty_order_tas.text = productToko.qty.toString()
                harga = productToko.harga_ecer!!.toInt() * productToko.qty!!.toInt()
                tv_harga_produk_tas.text = Utility.setFormatRupiah(harga)
                onClick.clickPlus()
            }
        }
    }

    interface OnClick{
        fun clickPlus()
        fun clickMinus()
    }

}