package com.kreasikode.qta1.global

import com.kreasikode.qta1.model.ItemOrder
import java.util.ArrayList

object Constants {

    const val DATABASE_NAME = "qtyfarm_database"
    const val PREF_MEMBER_DATA = "pref_member_data"
    const val PREF_MASTER_TOKEN = "pref_master_token"

}