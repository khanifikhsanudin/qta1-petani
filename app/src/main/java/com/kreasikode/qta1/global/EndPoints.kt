package com.kreasikode.qta1.global

object EndPoints {

    private const val BASE_URL          = "http://159.89.201.14/petani_api/public/api/"
    private const val COMPANY_URL       = "https://report.qta1.com/api/v1/"
    private const val SSO_URL           = "https://sso.qta1.com/sso/"
    const val CLIENT_KEY                = "0edbf20a.3db6bc.b5d539.b7cd01.f0cc99"
    const val ACCESS_TOKEN              = "110323129a58881615159113a731c30948d272bd"
    const val BASE_URL_IMAGE            = "https://server.qta1.com/public/"
    const val BASE_IMAGE_BACK            = "https://data-backend.qta1.com/"
    const val BASE_URL_IMAGE_NEW        = "https://qta1.com/"
    const val LOGIN                     = "${BASE_URL}login/apps"
    const val REGISTER                  = "${BASE_URL}members/store"
    const val COMPANY_VERB      = COMPANY_URL
    const val VERB_MASTER_LAHAN         = "get_master_lahan_wilayah"
    const val VERB_MASTER_MITRA         = "get_master_mitra_wilayah"
    const val VERB_MONITORING_BUDIDAYA  = "get_monitoring_budidaya"
    const val VERB_INVESTOR_OVERVIEW    = "get_investor_overview"
    const val VERB_INVESTOR_SPK         = "get_spk_by_investor"
    const val VERB_BODY_SPK             = "get_bdy_by_spk"
    const val VERB_BUDIDAYA_BODY        = "get_barjas_by_bdy"
    const val VERB_MASTER_ARTIKEL       = "get_master_artikel"
    const val VERB_MASTER_PRODUK        = "get_master_produk"
    const val VERB_MASTER_VENDOR        = "get_master_vendor_merchant"
    const val SSO_RESOURCE              = "${SSO_URL}resource"
}