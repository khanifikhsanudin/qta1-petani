package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArtikelFarm(
    val title: String,
    val img: Int,
    val description: String,
    val writer:String,
    val tanggal: String,
    val time_recent: String
) : Parcelable