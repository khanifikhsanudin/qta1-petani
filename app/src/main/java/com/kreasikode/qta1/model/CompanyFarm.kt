package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CompanyFarm(
    val investor_name: String,
    val address: String,
    val telp: String,
    val company_name: String,
    val company_img: Int?,
    val company_description: String
) : Parcelable