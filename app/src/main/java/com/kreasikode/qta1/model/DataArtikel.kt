package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataArtikel(
    val id            : String?,
    val original_id   : String?,
    val agency        : String?,
    val title         : String?,
    val content       : String?,
    val excerpt       : String?,
    val kategori      : String?,
    val is_active     : String?,
    val publish_at    : String?,
    val modified_at   : String?,
    val created_at    : String?,
    val updated_at    : String?,
    val deleted_at    : String?,
    val thumbnail_img : String?
) : Parcelable