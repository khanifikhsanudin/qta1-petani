package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataBodySpk(
    val budidaya_id: String? = null,
    val investor: String? = null,
    val spk_id: String? = null,
    val nomor_spk: String? = null,
    val nom_investasi_budidaya: String? = null,
    val serapan: String? = null,
    val sisa_per_budidaya: String? = null,
    val nomor_kontrak: String? = null,
    val reg_budidaya: String? = null,
    val provinsi: String? = null,
    val kabupaten: String? = null,
    val kecamatan: String? = null,
    val kelurahan: String? = null,
    val pencairan_barang: String? = null,
    val pencairan_jasa: String? = null,
    val pembina: String? = null,
    val mitra: String? = null
) : Parcelable