package com.kreasikode.qta1.model

data class DataBudidayaBody(
    val budidaya_id: String?,
    val reg_budidaya: String?,
    val nomor_kontrak: String?,
    val tanggal: String?,
    val kategori_budidaya_sop_id: String?,
    val proses: String?,
    val vendor_id: String?,
    val vendor: String?,
    val jenis: String?,
    val barang_jasa_id: String?,
    val nama: String?,
    val nominal: String?,
    val jumlah: String?,
    val satuan: String?,
    val keterangan: String?,
    val permasalahan: String?,
    val solusi: String?,
    val gambar_1: String?,
    val gambar_2: String?
)