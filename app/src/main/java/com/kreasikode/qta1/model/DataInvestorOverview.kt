package com.kreasikode.qta1.model

data class DataInvestorOverview (
    val investor_id: String?,
    val jml_spk: String?,
    val jml_kontrak_budidaya: String?,
    val spk_nominal_total: String?,
    val spk_nominal_terpakai: String?,
    val sisa_total_spk: String?,
    val jml_budidaya: String?,
    val jml_lahan: String?,
    val jml_mitra: String?,
    val jml_pembina: String?,
    val serapan_budidaya: String?
)
