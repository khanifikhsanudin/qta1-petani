package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataInvestorSpk(
    val spk_id: String?,
    val user_id: String?,
    val nomor_spk: String?,
    val nominal: String?,
    val file: String?,
    val tanggal: String?
) : Parcelable