package com.kreasikode.qta1.model

data class DataLahan(
    val provinsi_id: String?,
    val provinsi: String?,
    val jml_lahan_provinsi: Int?,
    val luas_lahan_provinsi: String?,
    val kabupaten_id: String?,
    val kabupaten: String?,
    val jml_lahan_kabupaten: Int?,
    val luas_lahan_kabupaten: String?,
    val kecamatan_id: String?,
    val kecamatan: String?,
    val jml_lahan_kecamatan: Int?,
    val luas_lahan_kecamatan: String?,
    val kelurahan_id: String?,
    val kelurahan: String?,
    val jml_lahan_kelurahan: Int?,
    val luas_lahan_kelurahan: String?
)