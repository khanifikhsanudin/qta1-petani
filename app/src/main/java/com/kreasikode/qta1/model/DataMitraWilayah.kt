package com.kreasikode.qta1.model

data class DataMitraWilayah(
    val provinsi_id: String?,
    val provinsi: String?,
    val jml_mitra_provinsi: Int?,
    val kabupaten_id: String?,
    val kabupaten: String?,
    val jml_mitra_kabupaten: Int?,
    val kecamatan_id: String?,
    val kecamatan: String?,
    val jml_mitra_kecamatan: Int?,
    val kelurahan_id: String?,
    val kelurahan: String?,
    val jml_mitra_kelurahan: Int?
)