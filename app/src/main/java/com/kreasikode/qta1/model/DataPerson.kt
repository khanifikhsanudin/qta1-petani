package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataPerson(
    val id          : String?,
    val name        : String?,
    val alamat      : String?,
    val notelp      : String?,
    val email       : String?,
    val kecamatan   : String?,
    val kota        : String?,
    val provinsi    : String?
) : Parcelable