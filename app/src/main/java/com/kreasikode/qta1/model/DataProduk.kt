package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataProduk(
    val id                   : String?,
    val nama_produk          : String?,
    val deskripsi            : String?,
    val kategory_budidaya_id : String?,
    val satuan               : String?,
    val harga_ecer           : String?,
    val harga_grosir         : String?,
    val min_grosir           : String?,
    val created_at           : String?,
    val updated_at           : String?,
    val deleted_at           : String?,
    val file_path            : String?,
    val upload_at            : String?,
    val kategori_budidaya    : String?,
    var qty :Int?
) : Parcelable