package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataVendorMerchant(
    val id               : String?,
    val provinsi_id      : String?,
    val kabupaten_id     : String?,
    val kecamatan_id     : String?,
    val kelurahan_id     : String?,
    val vendor           : String?,
    val bank             : String?,
    val nomor_rekening   : String?,
    val nama_rekening    : String?,
    val nomor_kartu_tani : String?,
    val created_at       : String?,
    val updated_at       : String?,
    val foto_path        : String?,
    val vendor_type      : String?,
    val alamat           : String?,
    val deskripsi        : String?,
    val provinsi         : String?,
    val kabupaten        : String?,
    val kecamatan        : String?,
    val kelurahan        : String?
) : Parcelable