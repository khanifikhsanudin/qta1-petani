package com.kreasikode.qta1.model

data class ItemOrder (
    val id: Int,
    val quantity: Int,
    val produk: DataProduk
)