package com.kreasikode.qta1.model

data class MasterToken(
    val accessToken: String?,
    val accessTokenExpirationTime: Long?,
    val refreshToken: String?,
    val authorizationCode: String?,
    val clientId: String?,
    val codeVerifier: String?,
    val grantType: String?,
    val scope: String?,
    val tokenType: String?
)