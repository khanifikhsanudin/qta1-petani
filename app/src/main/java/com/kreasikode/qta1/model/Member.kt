package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Member(
    val id: Int,
    val user_id_qta1: Int?,
    val nama_lengkap_qta1: String,
    val name: String,
    val timezone: String?,
    val username: String?,
    val email: String?,
    val role: String?,
    val refresh_token: String?,
    val expires: String?
) : Parcelable