package com.kreasikode.qta1.model

data class Meta(
    val code: Int?,
    val message: String?,
    val error: Boolean?
)