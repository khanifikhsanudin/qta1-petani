package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MonitoringBudidaya(
    val nama_lengkap: String?,
    val nomor_spk: String?,
    val nomor_registrasi_budidaya: String?,
    val proses: String?,
    val tanggal: String?,
    val budidaya_id: String?,
    val vendor: String?,
    val jenis_pengadaan: String?,
    val kategori_budidaya_sop_id: String?,
    val nama: String?,
    val barang_jasa_id: String?,
    val vendor_id: String?,
    val keterangan: String?,
    val satuan: String?,
    val nominal: String?,
    val jumlah: String?,
    val permasalahan: String?,
    val solusi: String?,
    val pembina: String?,
    val mitra: String?,
    val provinsi: String?,
    val kabupaten: String?,
    val kecamatan: String?,
    val kelurahan: String?,
    val kode_lahan: String?,
    val gambar_1: String?,
    val gambar_2: String?
) : Parcelable