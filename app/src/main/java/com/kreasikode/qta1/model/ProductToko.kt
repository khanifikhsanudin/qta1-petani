package com.kreasikode.qta1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductToko(
    val id: Int,
    val product_title: String,
    val product_img: Int,
    val product_name: String,
    val product_price: Int,
    val product_satuan: String,
    val product_description: String
) : Parcelable