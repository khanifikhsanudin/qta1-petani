package com.kreasikode.qta1.ui.aksespasar

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.CompanyAdapter
import com.kreasikode.qta1.model.DataVendorMerchant
import com.kreasikode.qta1.ui.detail.aksespasar.DetailAksesPasarActivity
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_akses_pasar.*

class AksesPasarActivity : AppCompatActivity(), AksesPasarView.MainView {

    private lateinit var presenter: AksesPasarPresenter
    private lateinit var companyAdapter: CompanyAdapter
    private var companyFarmBefore = emptyList<DataVendorMerchant>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_akses_pasar)
        presenter = AksesPasarPresenter(this, this)
        navigation_back.setOnClickListener { finish() }

        companyAdapter = CompanyAdapter {
            startActivity(DetailAksesPasarActivity.createIntent(this, it))
            Utility.startTransition(this)
        }
        rv_company.layoutManager = LinearLayoutManager(this)
        rv_company.adapter = companyAdapter

//        val companyFarmList = mutableListOf<CompanyFarm>(
//            CompanyFarm(
//                "Bayu Aji",
//                "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
//                "087715161160",
//                "Bulog divre katsuro", R.drawable.img_bulog,
//                "Bulog divre kartosuro adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
//            ), CompanyFarm(
//                "Karunia Arif",
//                "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
//                "087715161160",
//                "Bulog divre A", R.drawable.img_bulog,
//                "Bulog divre A adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
//            ), CompanyFarm(
//                "Dwiana Ardi",
//                "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
//                "087715161160",
//                "Bulog divre B", R.drawable.img_bulog,
//                "Bulog divre B adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
//            ), CompanyFarm(
//                "Galih Aprianto",
//                "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
//                "087715161160",
//                "Bulog divre C", R.drawable.img_bulog,
//                "Bulog divre C adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
//            )
//        )

        ed_cari.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            @SuppressLint("DefaultLocale")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) =
                if(ed_cari.text.toString().trim() != "") {
                    val listFilterCompanyFarm = mutableListOf<DataVendorMerchant>()
                    for (companyFarm: DataVendorMerchant in companyFarmBefore) {
                        if(companyFarm.vendor!!.toLowerCase().contains(ed_cari.text.toString().trim().toLowerCase())) {
                            listFilterCompanyFarm.add(companyFarm)
                        }
                    }
                    if(listFilterCompanyFarm.isNullOrEmpty().not()) {
                        companyAdapter.getData().equals(listFilterCompanyFarm).not().let {
                            if(it) {
                                companyAdapter.setData(listFilterCompanyFarm)
                            }
                        }
                    }else {

                    }
                }else {
                    companyAdapter.setData(companyFarmBefore)
                }
        })

        onStartProgress()
        presenter.fetchVendorMerchant()
    }

    override fun onStartProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onVendorMerchantFetched(listVendorMerchant: List<DataVendorMerchant>?) {
        onStopProgress()
        if (listVendorMerchant.isNullOrEmpty().not()) {
            companyAdapter.setData(listVendorMerchant!!)
            companyFarmBefore = listVendorMerchant
        }else {
            Toast.makeText(this, "Vendor tidak tersedia saat ini", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onFailedFetchVendorMerchant(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi Internet Offline", Toast.LENGTH_SHORT).show()
    }
}
