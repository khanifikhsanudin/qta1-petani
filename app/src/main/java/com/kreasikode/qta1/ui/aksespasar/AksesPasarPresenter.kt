package com.kreasikode.qta1.ui.aksespasar

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class AksesPasarPresenter(val context: Context, val mainView: AksesPasarView.MainView): AksesPasarView.MainPresenter {
    override fun fetchVendorMerchant() {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_MASTER_VENDOR)
                .build().getAsObject(
                    AksesPasarResponse::class.java,
                    object : ParsedRequestListener<AksesPasarResponse> {
                        override fun onResponse(response: AksesPasarResponse) {
                            mainView.onVendorMerchantFetched(response.data)
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedFetchVendorMerchant(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}