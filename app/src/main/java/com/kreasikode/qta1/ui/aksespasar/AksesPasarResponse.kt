package com.kreasikode.qta1.ui.aksespasar

import com.kreasikode.qta1.model.DataVendorMerchant

data class AksesPasarResponse(
    val data: List<DataVendorMerchant>?
)