package com.kreasikode.qta1.ui.aksespasar

import com.kreasikode.qta1.model.DataVendorMerchant

class AksesPasarView {

    interface MainPresenter {
        fun fetchVendorMerchant()
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onVendorMerchantFetched(listVendorMerchant: List<DataVendorMerchant>?)
        fun onFailedFetchVendorMerchant(message: String?)
        fun onOffline()
    }
}