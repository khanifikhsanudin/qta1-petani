package com.kreasikode.qta1.ui.artikel

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.ArtikelAdapter
import com.kreasikode.qta1.adapter.ArtikelSliderAdapter
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.DataArtikel
import com.kreasikode.qta1.ui.detail.artikel.DetailArtikelActivity
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_artikel.*

class ArtikelActivity : AppCompatActivity(), ArtikelView.MainView {

    lateinit var presenter: ArtikelPresenter
    lateinit var artikelSliderAdapter: ArtikelSliderAdapter
    lateinit var artikelAdapter: ArtikelAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artikel)
        presenter = ArtikelPresenter(this, this)

        navigation_back.setOnClickListener { finish() }

        artikelSliderAdapter = ArtikelSliderAdapter {
            startActivity(DetailArtikelActivity.createIntent(this, it))
            Utility.startTransition(this)
        }
        csv_artikel.adapter = artikelSliderAdapter

//        val listArtikelSlider = mutableListOf<ArtikelFarm>(
//            ArtikelFarm(
//                "Investasi Jagung Untuk Ternak",
//                R.drawable.img_product_d,
//                "Jagung pipil merupakan salah satu jagung yang banyak digunakan sebagai bahan pakan ternak. Sisa dari hasil panen biasanya digunakan sebagai pakan ternak, akan tetapi jagung pipil yang telah diolah dapat dijadikan sebagai pakan utama ternak. Budidaya jagung pipil sama dengan budidaya jagung manis. Waktu yang dibutuhkan dalam budidaya sekitar 60-70 hst. Biasanya satu tanaman jagung akan menghasilkan satu tongkol produktif, namun beberapa varietas unggul mampu menghasilkan lebih dari satu tongkol jagung produktif. Ciri-ciri jagung yang sudah bisa dipanen adalah biji jagung yang tidak meninggalkan bekas bila di tekan menggunakan kuku. Budidaya jagung pipil ini akan memberikan dampak zero waste karena semua hasil budidaya akan dimanfaatkan dengan cara diolah sebagai pakan ternak. Berbeda dengan jagung manis yang sering digunakan sebagai bahan industry atau bahan olahan masakan.\n" +
//                        "\n" +
//                        "Untuk mendukung kegiatan budidaya jagung pipil, Myagro sebagai platform investasi pertanian ingin mengajak anda untuk berinvestasi dalam mengembangkan jagung pipil di Indonesia. Investasi ini akan banyak membantu petani dalam berusaha tani dan juga tentunya akan menguntungkan investor yang telah berinvestasi di program ini. Investor akan mendapatkan keuntungan sebesar 15%-30% setiap satu tahun dengan investasi sebesar Rp 1.000.000 dalam masa kontrak 1 tahun. Petani akan mendapat bimbingan serta pendampingan dalam menjalankan budidaya serta usahatani. Sehingga program tersebut dapat membantu proses pemerataan ekonomi dan menambah ilmu serta keahlian bagi para petani di desa. Dengan teknologi pertanian organik serta integrated farming system semua hasil panen akan dimanfaatkan sehingga akan menghasilkan manfaat dan melancarkan program zero waste yang dicanangkan oleh Myagro. Resiko yang ditanggung sangat kecil persentasenya, karena Myagro telah melakukan berbagai riset sebelum menawarkan investasi pertanian kepada investor. Update realtime juga akan dilakukan oleh pihak Myagro kepada investor untuk mengetahui bagaimana perkembangan yang sudah berjalan. Semoga dengan adanya investasi ini produksi jagung pipil bisa meningkat dan mampu membantu masalah petani di Indonesia.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Cara Membuat Pupuk Sendiri untuk Tanaman Jagung Anda",
//                R.drawable.img_main_artikel,
//                "Sebelum Anda bersiap untuk membuat pupuk untuk tanaman, tentunya ada bahan-bahan yang harus Anda siapkan seperti berikut.\n" +
//                        "\n" +
//                        "    1 karung kotoran ayam, sapi, atau kambing\n" +
//                        "    ½ karung dedak\n" +
//                        "    30 kg tanaman seperti jerami, gedebog pisang atau bisa juga daun leguminosa\n" +
//                        "    100 gram gula merah\n" +
//                        "    50 ml bioaktivator atau biasa disebut EM4\n" +
//                        "    Air bersih\n" +
//                        "    Tong plastik ukuran 100 L\n" +
//                        "    Selang aerator transparan dengan diameter kira-kira 0,5 cm\n" +
//                        "    Botol plastik bekas ari mineral ukuran 1 liter\n" +
//                        "\n" +
//                        "Cara membuat\n" +
//                        "\n" +
//                        "    - Siapkan tong plastik untuk wadah pembuatan pupuk. Lubangi tutup tong tadi seukuran dengan selang aerator.\n" +
//                        "    - Potong bahan-bahan organik yang ada seperti dedaunan untuk dijadikan bahan baku.\n" +
//                        "    - Masukkan semua ke tong dan beri air secukupnya dengan perbandingan bahan pupuk lebih banyak sekitar 1: 2, aduk-aduk hingga merata.\n" +
//                        "    - Larutkan bioaktivator seperti EM4 dan juga gula merah dengan 5 liter air, aduk larutan hingga merata.\n" +
//                        "    - Masukkan adonan EM4 ke tong yang berisi bahan baku tadi, lalu tutup tong dengan rapat dan masukkan selang lewat tutup tong di lubang. Beri perekat pada selang masuk sehingga tidak ada udara yang masuk. Biarkan saja ujung selang yang lain masuk ke botol air mineral yang telah diberi air, tunggu hingga 7 sampai 10 hari.\n" +
//                        "    - Untuk mengecek tingkat kematangan, Anda hanya perlu membuka penutup tong dan cium bau adonan. Jika wanginya sama seperti tape, ini artinya adonan sudah matang.\n" +
//                        "    - Pisahkan zat cairan dengan ampasnya, hal tersebut bisa dengan cara menyaring adonannya.\n" +
//                        "    - Masukkan lagi sebuah cairan tadi yang telah melewati penyaringan pada sebuah botol plastik ataupun juga kaca, tutup rapat kembali.\n" +
//                        "    - Pupuk organik cair siap digunakan.\n",
//                "Pertanian ku",
//                "22 Febuari 2019",
//                "1 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            )
//        )

        artikelAdapter = ArtikelAdapter {
            startActivity(DetailArtikelActivity.createIntent(this, it))
            Utility.startTransition(this)
        }
        rv_artikel.layoutManager = LinearLayoutManager(this)
        rv_artikel.adapter = artikelAdapter

//        val listArtikel = mutableListOf<ArtikelFarm>(
//            ArtikelFarm(
//                "Cara Membuat Pupuk Sendiri untuk Tanaman Jagung Anda",
//                R.drawable.img_main_artikel,
//                "Sebelum Anda bersiap untuk membuat pupuk untuk tanaman, tentunya ada bahan-bahan yang harus Anda siapkan seperti berikut.\n" +
//                        "\n" +
//                        "    1 karung kotoran ayam, sapi, atau kambing\n" +
//                        "    ½ karung dedak\n" +
//                        "    30 kg tanaman seperti jerami, gedebog pisang atau bisa juga daun leguminosa\n" +
//                        "    100 gram gula merah\n" +
//                        "    50 ml bioaktivator atau biasa disebut EM4\n" +
//                        "    Air bersih\n" +
//                        "    Tong plastik ukuran 100 L\n" +
//                        "    Selang aerator transparan dengan diameter kira-kira 0,5 cm\n" +
//                        "    Botol plastik bekas ari mineral ukuran 1 liter\n" +
//                        "\n" +
//                        "Cara membuat\n" +
//                        "\n" +
//                        "    - Siapkan tong plastik untuk wadah pembuatan pupuk. Lubangi tutup tong tadi seukuran dengan selang aerator.\n" +
//                        "    - Potong bahan-bahan organik yang ada seperti dedaunan untuk dijadikan bahan baku.\n" +
//                        "    - Masukkan semua ke tong dan beri air secukupnya dengan perbandingan bahan pupuk lebih banyak sekitar 1: 2, aduk-aduk hingga merata.\n" +
//                        "    - Larutkan bioaktivator seperti EM4 dan juga gula merah dengan 5 liter air, aduk larutan hingga merata.\n" +
//                        "    - Masukkan adonan EM4 ke tong yang berisi bahan baku tadi, lalu tutup tong dengan rapat dan masukkan selang lewat tutup tong di lubang. Beri perekat pada selang masuk sehingga tidak ada udara yang masuk. Biarkan saja ujung selang yang lain masuk ke botol air mineral yang telah diberi air, tunggu hingga 7 sampai 10 hari.\n" +
//                        "    - Untuk mengecek tingkat kematangan, Anda hanya perlu membuka penutup tong dan cium bau adonan. Jika wanginya sama seperti tape, ini artinya adonan sudah matang.\n" +
//                        "    - Pisahkan zat cairan dengan ampasnya, hal tersebut bisa dengan cara menyaring adonannya.\n" +
//                        "    - Masukkan lagi sebuah cairan tadi yang telah melewati penyaringan pada sebuah botol plastik ataupun juga kaca, tutup rapat kembali.\n" +
//                        "    - Pupuk organik cair siap digunakan.\n",
//                "Pertanian ku",
//                "22 Febuari 2019",
//                "1 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Cara Membuat Pupuk Sendiri untuk Tanaman Jagung Anda",
//                R.drawable.img_main_artikel,
//                "Sebelum Anda bersiap untuk membuat pupuk untuk tanaman, tentunya ada bahan-bahan yang harus Anda siapkan seperti berikut.\n" +
//                        "\n" +
//                        "    1 karung kotoran ayam, sapi, atau kambing\n" +
//                        "    ½ karung dedak\n" +
//                        "    30 kg tanaman seperti jerami, gedebog pisang atau bisa juga daun leguminosa\n" +
//                        "    100 gram gula merah\n" +
//                        "    50 ml bioaktivator atau biasa disebut EM4\n" +
//                        "    Air bersih\n" +
//                        "    Tong plastik ukuran 100 L\n" +
//                        "    Selang aerator transparan dengan diameter kira-kira 0,5 cm\n" +
//                        "    Botol plastik bekas ari mineral ukuran 1 liter\n" +
//                        "\n" +
//                        "Cara membuat\n" +
//                        "\n" +
//                        "    - Siapkan tong plastik untuk wadah pembuatan pupuk. Lubangi tutup tong tadi seukuran dengan selang aerator.\n" +
//                        "    - Potong bahan-bahan organik yang ada seperti dedaunan untuk dijadikan bahan baku.\n" +
//                        "    - Masukkan semua ke tong dan beri air secukupnya dengan perbandingan bahan pupuk lebih banyak sekitar 1: 2, aduk-aduk hingga merata.\n" +
//                        "    - Larutkan bioaktivator seperti EM4 dan juga gula merah dengan 5 liter air, aduk larutan hingga merata.\n" +
//                        "    - Masukkan adonan EM4 ke tong yang berisi bahan baku tadi, lalu tutup tong dengan rapat dan masukkan selang lewat tutup tong di lubang. Beri perekat pada selang masuk sehingga tidak ada udara yang masuk. Biarkan saja ujung selang yang lain masuk ke botol air mineral yang telah diberi air, tunggu hingga 7 sampai 10 hari.\n" +
//                        "    - Untuk mengecek tingkat kematangan, Anda hanya perlu membuka penutup tong dan cium bau adonan. Jika wanginya sama seperti tape, ini artinya adonan sudah matang.\n" +
//                        "    - Pisahkan zat cairan dengan ampasnya, hal tersebut bisa dengan cara menyaring adonannya.\n" +
//                        "    - Masukkan lagi sebuah cairan tadi yang telah melewati penyaringan pada sebuah botol plastik ataupun juga kaca, tutup rapat kembali.\n" +
//                        "    - Pupuk organik cair siap digunakan.\n",
//                "Pertanian ku",
//                "22 Febuari 2019",
//                "1 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Investasi Jagung Untuk Ternak",
//                R.drawable.img_product_d,
//                "Jagung pipil merupakan salah satu jagung yang banyak digunakan sebagai bahan pakan ternak. Sisa dari hasil panen biasanya digunakan sebagai pakan ternak, akan tetapi jagung pipil yang telah diolah dapat dijadikan sebagai pakan utama ternak. Budidaya jagung pipil sama dengan budidaya jagung manis. Waktu yang dibutuhkan dalam budidaya sekitar 60-70 hst. Biasanya satu tanaman jagung akan menghasilkan satu tongkol produktif, namun beberapa varietas unggul mampu menghasilkan lebih dari satu tongkol jagung produktif. Ciri-ciri jagung yang sudah bisa dipanen adalah biji jagung yang tidak meninggalkan bekas bila di tekan menggunakan kuku. Budidaya jagung pipil ini akan memberikan dampak zero waste karena semua hasil budidaya akan dimanfaatkan dengan cara diolah sebagai pakan ternak. Berbeda dengan jagung manis yang sering digunakan sebagai bahan industry atau bahan olahan masakan.\n" +
//                        "\n" +
//                        "Untuk mendukung kegiatan budidaya jagung pipil, Myagro sebagai platform investasi pertanian ingin mengajak anda untuk berinvestasi dalam mengembangkan jagung pipil di Indonesia. Investasi ini akan banyak membantu petani dalam berusaha tani dan juga tentunya akan menguntungkan investor yang telah berinvestasi di program ini. Investor akan mendapatkan keuntungan sebesar 15%-30% setiap satu tahun dengan investasi sebesar Rp 1.000.000 dalam masa kontrak 1 tahun. Petani akan mendapat bimbingan serta pendampingan dalam menjalankan budidaya serta usahatani. Sehingga program tersebut dapat membantu proses pemerataan ekonomi dan menambah ilmu serta keahlian bagi para petani di desa. Dengan teknologi pertanian organik serta integrated farming system semua hasil panen akan dimanfaatkan sehingga akan menghasilkan manfaat dan melancarkan program zero waste yang dicanangkan oleh Myagro. Resiko yang ditanggung sangat kecil persentasenya, karena Myagro telah melakukan berbagai riset sebelum menawarkan investasi pertanian kepada investor. Update realtime juga akan dilakukan oleh pihak Myagro kepada investor untuk mengetahui bagaimana perkembangan yang sudah berjalan. Semoga dengan adanya investasi ini produksi jagung pipil bisa meningkat dan mampu membantu masalah petani di Indonesia.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Cara Membuat Pupuk Sendiri untuk Tanaman Jagung Anda",
//                R.drawable.img_main_artikel,
//                "Sebelum Anda bersiap untuk membuat pupuk untuk tanaman, tentunya ada bahan-bahan yang harus Anda siapkan seperti berikut.\n" +
//                        "\n" +
//                        "    1 karung kotoran ayam, sapi, atau kambing\n" +
//                        "    ½ karung dedak\n" +
//                        "    30 kg tanaman seperti jerami, gedebog pisang atau bisa juga daun leguminosa\n" +
//                        "    100 gram gula merah\n" +
//                        "    50 ml bioaktivator atau biasa disebut EM4\n" +
//                        "    Air bersih\n" +
//                        "    Tong plastik ukuran 100 L\n" +
//                        "    Selang aerator transparan dengan diameter kira-kira 0,5 cm\n" +
//                        "    Botol plastik bekas ari mineral ukuran 1 liter\n" +
//                        "\n" +
//                        "Cara membuat\n" +
//                        "\n" +
//                        "    - Siapkan tong plastik untuk wadah pembuatan pupuk. Lubangi tutup tong tadi seukuran dengan selang aerator.\n" +
//                        "    - Potong bahan-bahan organik yang ada seperti dedaunan untuk dijadikan bahan baku.\n" +
//                        "    - Masukkan semua ke tong dan beri air secukupnya dengan perbandingan bahan pupuk lebih banyak sekitar 1: 2, aduk-aduk hingga merata.\n" +
//                        "    - Larutkan bioaktivator seperti EM4 dan juga gula merah dengan 5 liter air, aduk larutan hingga merata.\n" +
//                        "    - Masukkan adonan EM4 ke tong yang berisi bahan baku tadi, lalu tutup tong dengan rapat dan masukkan selang lewat tutup tong di lubang. Beri perekat pada selang masuk sehingga tidak ada udara yang masuk. Biarkan saja ujung selang yang lain masuk ke botol air mineral yang telah diberi air, tunggu hingga 7 sampai 10 hari.\n" +
//                        "    - Untuk mengecek tingkat kematangan, Anda hanya perlu membuka penutup tong dan cium bau adonan. Jika wanginya sama seperti tape, ini artinya adonan sudah matang.\n" +
//                        "    - Pisahkan zat cairan dengan ampasnya, hal tersebut bisa dengan cara menyaring adonannya.\n" +
//                        "    - Masukkan lagi sebuah cairan tadi yang telah melewati penyaringan pada sebuah botol plastik ataupun juga kaca, tutup rapat kembali.\n" +
//                        "    - Pupuk organik cair siap digunakan.\n",
//                "Pertanian ku",
//                "22 Febuari 2019",
//                "1 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            )
//        )
//        artikelAdapter.setData(listArtikel)
        onStart()
        presenter.fetchArtikel()
    }

    private fun resolveArtikel(listArtikel: List<DataArtikel>?) {
        if (listArtikel.isNullOrEmpty().not()) {
            if (listArtikel!![0].thumbnail_img.isNullOrEmpty().not()) {
                ImageUtil.fromUrl(applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(listArtikel[0].thumbnail_img!!), iv_main_artikel)
            }else {
                ImageUtil.fromDrawable(applicationContext, ContextCompat.getDrawable(this, R.drawable.no_image)!!, iv_main_artikel)
            }
            tv_main_title.text = listArtikel[0].title
            cv_main_artikel.setOnClickListener {
                startActivity(DetailArtikelActivity.createIntent(this, listArtikel[0]))
                Utility.startTransition(this)
            }
            pb_featured.visibility = View.GONE

            if (listArtikel.size > 4) {
                artikelSliderAdapter.setData(listArtikel.subList(1, 4))
                csv_artikel.registerOnPageChangeCallback(object: com.github.islamkhsh.viewpager2.ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)
                        dot_1.setImageResource(R.drawable.default_dot)
                        dot_2.setImageResource(R.drawable.default_dot)
                        dot_3.setImageResource(R.drawable.default_dot)
//                dot_4.setImageResource(R.drawable.default_dot)
                        when(position) {
                            0 -> { dot_1.setImageResource(R.drawable.selected_dot) }
                            1 -> { dot_2.setImageResource(R.drawable.selected_dot) }
                            2 -> { dot_3.setImageResource(R.drawable.selected_dot) }
//                    3 -> { dot_4.setImageResource(R.drawable.selected_dot)  }
                        }
                    }
                })
                csv_artikel.setPageTransformer { page, position ->
                    val offset = position * -(2 * 10 + 20)
                    if (csv_artikel.orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                        if (ViewCompat.getLayoutDirection(csv_artikel) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                            page.translationX = -offset
                        } else {
                            page.translationX = offset
                        }
                    } else {
                        page.translationY = offset
                    }
                }

                artikelAdapter.setData(listArtikel.subList(4, listArtikel.size))
            }else {
                artikelAdapter.setData(listArtikel.subList(1, listArtikel.size))
            }
            pb_slider.visibility = View.GONE
            ll_dot.visibility = View.VISIBLE
        }else {
            Toast.makeText(this, "Artikel tidak tersedia saat ini", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStartProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onArtikelFetched(listArtikel: List<DataArtikel>?) {
        onStopProgress()
        if (listArtikel.isNullOrEmpty().not()) {
            resolveArtikel(listArtikel)
        }else {
            Toast.makeText(this, "Artikel tidak tersedia saat ini", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onFailedFetchArtikel(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi Internet Offline", Toast.LENGTH_SHORT).show()
    }
}

