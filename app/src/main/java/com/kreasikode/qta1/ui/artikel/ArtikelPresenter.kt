package com.kreasikode.qta1.ui.artikel

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class ArtikelPresenter(val context: Context, val mainView: ArtikelView.MainView): ArtikelView.MainPresenter {
    override fun fetchArtikel() {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_MASTER_ARTIKEL)
                .build().getAsObject(
                    ArtikelResponse::class.java,
                    object : ParsedRequestListener<ArtikelResponse> {
                        override fun onResponse(response: ArtikelResponse) {
                            mainView.onArtikelFetched(response.data)
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedFetchArtikel(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}