package com.kreasikode.qta1.ui.artikel

import com.kreasikode.qta1.model.DataArtikel

data class ArtikelResponse(
    val data: List<DataArtikel>?
)