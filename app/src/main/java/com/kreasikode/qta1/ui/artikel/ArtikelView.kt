package com.kreasikode.qta1.ui.artikel

import com.kreasikode.qta1.model.DataArtikel

class ArtikelView {

    interface MainPresenter {
        fun fetchArtikel()
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onArtikelFetched(listArtikel: List<DataArtikel>?)
        fun onFailedFetchArtikel(message: String?)
        fun onOffline()
    }
}