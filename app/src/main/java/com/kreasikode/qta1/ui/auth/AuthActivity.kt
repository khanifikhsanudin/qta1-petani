package com.kreasikode.qta1.ui.auth

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.Member
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.PreferencesManager
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity(), AuthView.MainView {

    private lateinit var presenter: AuthView.MainPresenter
    private lateinit var loading: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        presenter = AuthPresenter(this, this)
        loading = Utility.setProgressDialog(this, "Sedang Memuat...")
        navigation_back.setOnClickListener { finish() }
        ImageUtil.fromDrawable(this.applicationContext, ContextCompat.getDrawable(this, R.drawable.qty_logo)!!, iv_logo)
        changeTab(0)
        tab_login.setOnClickListener { changeTab(0) }
        tab_daftar.setOnClickListener { changeTab(1) }
        mbtn_login.setOnClickListener {
            validateInputLogin()
        }
        mbtn_daftar.setOnClickListener {
            validateInputRegister()
        }
    }

    private fun validateInputLogin() {
        val email = tifed_email.text.toString().trim()
        val password= tifed_password.text.toString().trim()

        if (Utility.isEmailValid(email).not()) {
            tifl_email.error = "Format Email Salah"
            tifed_email.requestFocus()
            return
        }

        if (password.length < 8) {
            tifl_password.error = "Password min. 8 Karakter"
            tifed_password.requestFocus()
            return
        }

        onStartProgress()
        presenter.doLogin(email, password)
    }

    private fun validateInputRegister() {
        val username = tifed_reg_username.text.toString().trim()
        val email = tifed_reg_email.text.toString().trim()
        val password= tifed_reg_password.text.toString().trim()
        val konfirmasi = tifed_reg_konfirmasi.text.toString().trim()

        if (username.length < 4) {
            tifl_reg_username.error = "Username min. 4 Karakter"
            tifed_reg_username.requestFocus()
            return
        }

        if (Utility.isEmailValid(email).not()) {
            tifl_reg_email.error = "Format Email Salah"
            tifed_reg_email.requestFocus()
            return
        }

        if (password.length < 8) {
            tifl_reg_password.error = "Password min. 8 Karakter"
            tifed_reg_password.requestFocus()
            return
        }

        if (konfirmasi != password) {
            tifl_reg_konfirmasi.error = "Konfirmasi Password Salah"
            tifed_reg_password.requestFocus()
            return
        }

        onStartProgress()
        presenter.doRegister(username, email, password)
    }

    private fun changeTab(position: Int) {
        when(position) {
            0 -> {
                tab_login.setBackgroundResource(R.color.colorPrimary)
                tab_login.setTextColor(ContextCompat.getColor(this, R.color.white))
                tab_login.elevation = 8f

                tab_daftar.setBackgroundResource(R.drawable.shape_rectangle)
                tab_daftar.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                tab_daftar.elevation = 0f
                ll_register.visibility = View.GONE
                ll_login.visibility = View.VISIBLE
            }
            1 -> {
                tab_daftar.setBackgroundResource(R.color.colorPrimary)
                tab_daftar.setTextColor(ContextCompat.getColor(this, R.color.white))
                tab_daftar.elevation = 8f

                tab_login.setBackgroundResource(R.drawable.shape_rectangle)
                tab_login.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                tab_login.elevation = 0f
                ll_register.visibility = View.VISIBLE
                ll_login.visibility = View.GONE
            }
        }
    }

    override fun onStartProgress() {
        loading.show()
    }

    override fun onStopProgress() {
        loading.dismiss()
    }

    override fun onLoginSuccesss(memberResponse: MemberResponse?) {
        onStopProgress()
        PreferencesManager.init(this).memberData = memberResponse?.member
        setResult(Activity.RESULT_OK, Intent())
        finish()
    }

    override fun onFailedLogin(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onRegisterSuccess(member: Member?) {
        onStopProgress()
        PreferencesManager.init(this).memberData = member
        setResult(Activity.RESULT_OK, Intent())
        finish()
    }

    override fun onFailedRegister(message: String?) {
        onStopProgress()
        if (message == "duplicate" || message == "parseError") {
            Toast.makeText(this, "Gagal: Email telah digunakan", Toast.LENGTH_SHORT).show()
        }else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi Internet Offline", Toast.LENGTH_SHORT).show()
    }
}
