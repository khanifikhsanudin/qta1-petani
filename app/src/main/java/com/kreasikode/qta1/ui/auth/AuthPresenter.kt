package com.kreasikode.qta1.ui.auth

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class AuthPresenter(var context: Context, var mainView: AuthView.MainView): AuthView.MainPresenter {
    override fun doLogin(email: String, password: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.LOGIN)
                .setPriority(Priority.HIGH)
                .addHeaders("client-key", EndPoints.CLIENT_KEY)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .build().getAsObject(
                    LoginResponse::class.java,
                    object : ParsedRequestListener<LoginResponse> {
                        override fun onResponse(response: LoginResponse) {
                            if(response.meta != null) {
                                if (response.meta.code == 200) {
                                    if (response.meta.error!!.not()) {
                                        mainView.onLoginSuccesss(response.data)
                                    }else {
                                        mainView.onFailedLogin(response.meta.message)
                                    }
                                } else {
                                    mainView.onFailedLogin(response.meta.message)
                                }
                            }else {
                                mainView.onFailedLogin("Unknown Error")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedLogin(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }

    override fun doRegister(username: String, email: String, password: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.REGISTER)
                .setPriority(Priority.HIGH)
                .addHeaders("client-key", EndPoints.CLIENT_KEY)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .addBodyParameter("display_name", username)
                .build().getAsObject(
                    RegisterResponse::class.java,
                    object : ParsedRequestListener<RegisterResponse> {
                        override fun onResponse(response: RegisterResponse) {
                            if (response.meta != null) {
                                if (response.meta.code == 200) {
                                    if (response.meta.error!!.not()) {
                                        mainView.onRegisterSuccess(response.data)
                                    }else {
                                        mainView.onFailedRegister(response.meta.message)
                                    }
                                } else {
                                    if(response.meta.code == 401) {
                                        mainView.onFailedRegister("duplicate")
                                    }else {
                                        mainView.onFailedRegister(response.meta.message)
                                    }
                                }
                            }else {
                                mainView.onFailedRegister("Unknown Error")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedRegister(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }

}