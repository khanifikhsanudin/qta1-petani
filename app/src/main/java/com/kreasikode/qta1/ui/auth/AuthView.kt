package com.kreasikode.qta1.ui.auth

import com.kreasikode.qta1.model.Member

class AuthView {

    interface MainPresenter {
        fun doLogin(email: String, password: String)
        fun doRegister(username: String, email: String, password: String)
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onLoginSuccesss(memberResponse: MemberResponse?)
        fun onFailedLogin(message: String?)
        fun onRegisterSuccess(member: Member?)
        fun onFailedRegister(message: String?)
        fun onOffline()
    }
}