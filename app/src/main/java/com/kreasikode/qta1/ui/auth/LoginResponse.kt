package com.kreasikode.qta1.ui.auth

import com.kreasikode.qta1.model.Meta

data class LoginResponse(
    val meta: Meta?,
    val data: MemberResponse?
)