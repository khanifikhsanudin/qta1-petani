package com.kreasikode.qta1.ui.auth

import com.kreasikode.qta1.model.Member

data class MemberResponse(
    val token: String?,
    val member: Member?
)