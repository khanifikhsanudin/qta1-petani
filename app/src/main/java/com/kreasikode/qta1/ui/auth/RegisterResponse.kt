package com.kreasikode.qta1.ui.auth

import com.kreasikode.qta1.model.Member
import com.kreasikode.qta1.model.Meta

data class RegisterResponse(
    val meta: Meta?,
    val data: Member?
)