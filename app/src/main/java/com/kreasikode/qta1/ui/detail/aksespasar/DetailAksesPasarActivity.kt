package com.kreasikode.qta1.ui.detail.aksespasar

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.Extras
import com.kreasikode.qta1.model.DataVendorMerchant
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_detail_akses_pasar.*

class DetailAksesPasarActivity : AppCompatActivity() {

    val companyFarm: DataVendorMerchant by lazy { intent.getParcelableExtra(Extras.EXTRA_COMPANY_FARM) as DataVendorMerchant }

    @SuppressLint("SetTextI18n", "DefaultLocale")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_akses_pasar)

        navigation_back.setOnClickListener { finish() ; Utility.finishTransition(this)}
        if (companyFarm.foto_path.isNullOrEmpty()) {
            ImageUtil.fromDrawable(applicationContext, ContextCompat.getDrawable(applicationContext, R.drawable.no_image)!!, iv_company)
        }else {
            ImageUtil.fromUrl(applicationContext, companyFarm.foto_path!!, iv_company)
        }
        tv_company_name.text = companyFarm.vendor
        tv_company_desc.text = companyFarm.deskripsi?:""
        tv_alamat.text = Utility.capitalizeWords("${companyFarm.kelurahan}, ${companyFarm.kecamatan} ${companyFarm.kabupaten} ${companyFarm.provinsi}")
        tv_investor_name.text = companyFarm.nomor_kartu_tani?:"Anonim"
        tv_telp.text = "-"
    }

    companion object {
        fun createIntent(context: Context, companyFarm: DataVendorMerchant): Intent {
            val intent = Intent(context, DetailAksesPasarActivity::class.java)
            intent.putExtra(Extras.EXTRA_COMPANY_FARM, companyFarm)
            return intent
        }
    }
}
