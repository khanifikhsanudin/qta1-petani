package com.kreasikode.qta1.ui.detail.artikel

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Spannable
import android.text.style.StyleSpan
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.core.text.toSpannable
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.OtherArtikelAdapter
import com.kreasikode.qta1.model.DataArtikel
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_detail_artikel.*
import kotlinx.android.synthetic.main.activity_detail_artikel.navigation_back

class DetailArtikelActivity : AppCompatActivity() {

    val artikelFarm: DataArtikel by lazy { intent.getParcelableExtra("DETAIL_ARTIKEL") as DataArtikel }

    lateinit var otherArtikelAdapter: OtherArtikelAdapter

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_artikel)

        navigation_back.setOnClickListener { finish() ; Utility.finishTransition(this)}

        if (artikelFarm.thumbnail_img.isNullOrEmpty()) {
            ImageUtil.fromDrawable(applicationContext, ContextCompat.getDrawable(applicationContext, R.drawable.no_image)!!, iv_artikel)
        }else {
            ImageUtil.fromUrl(applicationContext, artikelFarm.thumbnail_img!!, iv_artikel)
        }

        tv_artikel_title.text = artikelFarm.title
        tv_author_tanggal.text = "${artikelFarm.agency}, ${Utility.indoDateFormat(artikelFarm.publish_at?:"2020-03-04 03:15:42")}"

        val describe = HtmlCompat.fromHtml(artikelFarm.content?:"No description avaliable", HtmlCompat.FROM_HTML_MODE_LEGACY)
        describe.toSpannable().setSpan(StyleSpan(Typeface.NORMAL), 0, describe.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_deskripsi.text =describe.toString()

//        otherArtikelAdapter = OtherArtikelAdapter {
//            startActivity(createIntent(this, it))
//        }
//        rv_artikel_lainnya.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//        rv_artikel_lainnya.adapter = otherArtikelAdapter
//
//        val listArtikel = mutableListOf<ArtikelFarm>(
//            ArtikelFarm(
//                "Invest Untung",
//                R.drawable.img_product_d,
//                "Komoditas jagung sedang mengalami kenaikan maka",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Melihat Genetik Jagung Manis",
//                R.drawable.img_jagung_mais,
//                "Seperti tanaman jagung yang lainnya, tanaman jagung manis memiliki kromosom sebanyak 2n = 2x = 20. Berdasarkan sifat penyerbukannya, jagung manis termasuk tanaman menyerbuk silang karena mempunyai tipe seks monocious, yakni alat kelamin jantan dan betina terpisah pada bunga yang berbeda, tetapi masih pada satu individu tanaman. Varietas jagung manis terbagi menjadi dua, yaitu hibrida dan bersari bebas.\nJagung manis adalah hasil mutasi resesif yang terjadi secara alami di dalam gen yang mengontrol konversi gula menjadi pati dalam endosperm biji. Selama lebih dari 30 tahun yang lalu, alel sugary (su) yang terdapat pada kromosom no 4 dianggap sebagai gen pengendali rasa manis pada jagung manis. Saat ini, diketahui terdapat paling sedikit tujuh gen lain yang mempengaruhi sintesis karbohidrat pada endosperma yang digunakan secara tunggal maupun kombinasi beberapa gen dalam membentuk varietas jagung manis.\n" +
//                        "\n" +
//                        "Terdapat delapan gen resesif pada tanaman jagung manis yang dikembangkan untuk varietas komersial. Delapan gen tersebut, yaitu amylose-extender 1 (ae1), brittle 1 (bt1), brittle 2 (bt2), sugary 1 (su1), shrunken 2 (sh2), dull 1 (du1), sugary enhancer 1 (se1), dan waxy 1 (wx1). Delapan gen tersebut dibagi ke dalam dua kelompok berdasarkan pengaruhnya pada komposisi endosperm.\n" +
//                        "\n" +
//                        "Kelompok pertama adalah brittle 1 (bt1), brittle 2 (bt2), dan sh2. Kelompok ini mengakumulasi gula pada biji dan mereduksi pati pada saat menjadi benih (biji matang fisiologis). Pada umur 18 dan 21 hari setelah penyerbukan (tahap panen segar), kandungan gula total 4—8 kali dibandingkan jagung normal. Karena kandungan gulanya tinggi, kelompok ini sering digunakan untuk merakit varietas komersial jagung manis. Untuk jagung manis olahan, tipe sh2 menempati urutan kedua setelah tipe su1. Akan tetapi, untuk jagung manis segar tipe sh2 melebihi tipe su1. Karena kandungan gulanya tinggi, varietas-varietas tipe ini sering disebut super sweet atau extra sweet.",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Invest Untung",
//                R.drawable.img_product_d,
//                "Komoditas jagung sedang mengalami kenaikan maka",
//                "Andi Suro",
//                "20 Febuari 2019",
//                "4 jam lalu"
//            ), ArtikelFarm(
//                "Cara Membuat Pupuk Sendiri untuk Tanaman Jagung Anda",
//                R.drawable.img_main_artikel,
//                "Sebelum Anda bersiap untuk membuat pupuk untuk tanaman, tentunya ada bahan-bahan yang harus Anda siapkan seperti berikut.\n" +
//                        "\n" +
//                        "    1 karung kotoran ayam, sapi, atau kambing\n" +
//                        "    ½ karung dedak\n" +
//                        "    30 kg tanaman seperti jerami, gedebog pisang atau bisa juga daun leguminosa\n" +
//                        "    100 gram gula merah\n" +
//                        "    50 ml bioaktivator atau biasa disebut EM4\n" +
//                        "    Air bersih\n" +
//                        "    Tong plastik ukuran 100 L\n" +
//                        "    Selang aerator transparan dengan diameter kira-kira 0,5 cm\n" +
//                        "    Botol plastik bekas ari mineral ukuran 1 liter\n" +
//                        "\n" +
//                        "Cara membuat\n" +
//                        "\n" +
//                        "    - Siapkan tong plastik untuk wadah pembuatan pupuk. Lubangi tutup tong tadi seukuran dengan selang aerator.\n" +
//                        "    - Potong bahan-bahan organik yang ada seperti dedaunan untuk dijadikan bahan baku.\n" +
//                        "    - Masukkan semua ke tong dan beri air secukupnya dengan perbandingan bahan pupuk lebih banyak sekitar 1: 2, aduk-aduk hingga merata.\n" +
//                        "    - Larutkan bioaktivator seperti EM4 dan juga gula merah dengan 5 liter air, aduk larutan hingga merata.\n" +
//                        "    - Masukkan adonan EM4 ke tong yang berisi bahan baku tadi, lalu tutup tong dengan rapat dan masukkan selang lewat tutup tong di lubang. Beri perekat pada selang masuk sehingga tidak ada udara yang masuk. Biarkan saja ujung selang yang lain masuk ke botol air mineral yang telah diberi air, tunggu hingga 7 sampai 10 hari.\n" +
//                        "    - Untuk mengecek tingkat kematangan, Anda hanya perlu membuka penutup tong dan cium bau adonan. Jika wanginya sama seperti tape, ini artinya adonan sudah matang.\n" +
//                        "    - Pisahkan zat cairan dengan ampasnya, hal tersebut bisa dengan cara menyaring adonannya.\n" +
//                        "    - Masukkan lagi sebuah cairan tadi yang telah melewati penyaringan pada sebuah botol plastik ataupun juga kaca, tutup rapat kembali.\n" +
//                        "    - Pupuk organik cair siap digunakan.\n",
//                "Pertanian ku",
//                "22 Febuari 2019",
//                "1 jam lalu"
//            )
//        )
//        otherArtikelAdapter.setData(listArtikel)
    }

    companion object {

        fun createIntent(context: Context, artikelFarm: DataArtikel): Intent {
            val intent = Intent(context, DetailArtikelActivity::class.java)
            intent.putExtra("DETAIL_ARTIKEL", artikelFarm)
            return intent
        }
    }
}
