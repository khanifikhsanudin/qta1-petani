package com.kreasikode.qta1.ui.detail.member

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.Member
import com.kreasikode.qta1.utils.PreferencesManager
import kotlinx.android.synthetic.main.activity_detail_member.*

class DetailMemberActivity : AppCompatActivity(), DetailMemberView.MainView {

    private lateinit var presenter: DetailMemberPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_member)
        presenter = DetailMemberPresenter(this, this)

        navigation_back.setOnClickListener {
            setResult(Activity.RESULT_OK, Intent())
            finish()
        }

        mbtn_logout.setOnClickListener {
            PreferencesManager.init(this).also {
                it.memberData = null
                it.masterToken = null
            }
            setResult(Activity.RESULT_OK, Intent())
            finish()
        }

        val prefs = PreferencesManager.init(this)
        if (prefs.masterToken != null) {
            if (prefs.memberData == null) {
                layout_member.visibility = View.INVISIBLE
                onStartProgress()
                presenter.fetchUserDetail(prefs.masterToken!!.accessToken!!)
            }else {
                refreshUIMember(prefs.memberData!!)
            }
        }else {
            Toast.makeText(this, "Kesalahan sistem", Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    private fun refreshUIMember(data: Member) {
        tv_name.text = data.nama_lengkap_qta1
        tv_timezone.text = data.timezone
        tv_username.text = data.username
        tv_email.text = data.email
        tv_expires.text = data.expires
        tv_refresh_token.text = data.refresh_token
        layout_member.visibility = View.VISIBLE
    }

    override fun onStartProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onUserDetailFetched(member: Member) {
        onStopProgress()
        PreferencesManager.init(this).memberData = member
        refreshUIMember(member)
    }

    override fun onFailed(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi anda sedang offline", Toast.LENGTH_LONG).show()
    }
}
