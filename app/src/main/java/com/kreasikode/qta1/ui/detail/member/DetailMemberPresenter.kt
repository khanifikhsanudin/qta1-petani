package com.kreasikode.qta1.ui.detail.member

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.model.Member
import com.kreasikode.qta1.utils.NetworkUtil

class DetailMemberPresenter(
    private val context: Context,
    private val mainView: DetailMemberView.MainView
): DetailMemberView.MainPresenter {

    override fun fetchUserDetail(access_token: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.SSO_RESOURCE)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/x-www-form-urlencoded")
                .addBodyParameter("access_token", access_token)
                .build().getAsObject(
                    Member::class.java,
                    object : ParsedRequestListener<Member> {
                        override fun onResponse(response: Member?) {
                            if (response != null) {
                                mainView.onUserDetailFetched(response)
                            }else {
                                mainView.onFailed("Gagal mengambil data")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailed(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}