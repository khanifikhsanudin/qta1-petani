package com.kreasikode.qta1.ui.detail.member

import com.kreasikode.qta1.model.Member

class DetailMemberView {

    interface MainPresenter {
        fun fetchUserDetail(access_token: String)
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onUserDetailFetched(member: Member)
        fun onFailed(message: String?)
        fun onOffline()
    }
}