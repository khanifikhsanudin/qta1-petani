package com.kreasikode.qta1.ui.detail.toko

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.DataConst
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.global.Extras
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_detail_product.*

class DetailProductActivity : AppCompatActivity() {

    val productToko: DataProduk by lazy { intent.getParcelableExtra(Extras.EXTRA_PRODUCT_TOKO) as DataProduk }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)

        navigation_back.setOnClickListener { finish() ; Utility.finishTransition(this) ; Utility.finishTransition(this)}

        tv_appbar_title.text = productToko.nama_produk
        tv_product_name.text = productToko.nama_produk
        if (productToko.file_path.isNullOrEmpty()) {
            ImageUtil.fromDrawable(applicationContext, ContextCompat.getDrawable(applicationContext, R.drawable.no_image)!!, iv_product)
        }else {
            ImageUtil.fromUrl(applicationContext, EndPoints.BASE_URL_IMAGE_NEW.plus(productToko.file_path), iv_product)
        }

        tv_seller.text = "Petani Mitra\nQTA1"
        tv_deskripsi.text = productToko.deskripsi
        tv_price.text = Utility.setFormatRupiah(productToko.harga_ecer!!.toInt())
        tv_satuan.text = "/${productToko.satuan}"
        btn_buy.setOnClickListener {
            if (DataConst.DataOrder.isEmpty()){
                val item:DataProduk = productToko
                item.qty = tv_qty.text.toString().trim().toInt()
                DataConst.DataOrder.add(item)
            }else{
                val findItem = DataConst.DataOrder.find { it-> productToko.id.equals(it.id) }
                if (findItem?.id.isNullOrEmpty()){
                    val item:DataProduk = productToko
                    item.qty = tv_qty.text.toString().trim().toInt()
                    DataConst.DataOrder.add(item)
                }
            }
            Toast.makeText(applicationContext,"Produk masuk kedalam tas",
                Toast.LENGTH_SHORT).show()
        }

        tv_min.setOnClickListener {
            val qty = tv_qty.text.toString().trim().toInt()
            if(qty >= 1) {
                tv_qty.text ="${qty - 1}"
            }
        }
        tv_plus.setOnClickListener {
            val qty = tv_qty.text.toString().trim().toInt()
            tv_qty.text ="${qty + 1}"
        }
    }

    companion object {
        fun createIntent(context: Context, productToko: DataProduk): Intent {
            val intent = Intent(context, DetailProductActivity::class.java)
            intent.putExtra(Extras.EXTRA_PRODUCT_TOKO, productToko)
            return intent
        }
    }
}
