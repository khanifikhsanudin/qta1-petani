package com.kreasikode.qta1.ui.dialog


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager

import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.SpkChooserAdapter
import com.kreasikode.qta1.ui.monitoring.budidaya.ParentMonitoringBudidaya
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.fragment_dialog_spk_chooser.*

/**
 * A simple [Fragment] subclass.
 */
class DialogSpkChooserFragment(
    private val listItem: List<ParentMonitoringBudidaya>,
    private val onClickItem: (ParentMonitoringBudidaya) -> Unit
) : DialogFragment() {

    private lateinit var spkChosserAdapter: SpkChooserAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_spk_chooser, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spkChosserAdapter = SpkChooserAdapter {
            onClickItem(listItem[it])
        }
        rv_item.layoutManager = LinearLayoutManager(requireContext())
        rv_item.adapter = spkChosserAdapter

        val listString = mutableListOf<String>()
        for (i in listItem) {
            listString.add("${i.nomor_spk} - ${Utility.setFormatRupiah(i.total_nominal?:0)}")
        }

        spkChosserAdapter.setData(listString)
    }
}
