package com.kreasikode.qta1.ui.grafik

import com.github.mikephil.charting.formatter.ValueFormatter

class MyValueFormater(val labels: List<String?>): ValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        var l = ""
        for (label in labels.withIndex()) {
            if (label.index.toFloat() == value) {
                l = label.value?:"??"
            }
        }
        return  l
    }
}