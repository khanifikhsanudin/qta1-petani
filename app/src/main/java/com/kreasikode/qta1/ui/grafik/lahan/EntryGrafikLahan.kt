package com.kreasikode.qta1.ui.grafik.lahan

import com.github.mikephil.charting.data.BarEntry
import com.kreasikode.qta1.model.DataLahan


data class EntryGrafikLahan(
    val valueId: String,
    val valueDataLahan: DataLahan,
    val entryBar: BarEntry
)