package com.kreasikode.qta1.ui.grafik.lahan

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.DataLahan
import com.kreasikode.qta1.ui.grafik.MyValueFormater
import com.kreasikode.qta1.widget.CustomXAxisRenderer
import kotlinx.android.synthetic.main.activity_grafik_lahan.*

class GrafikLahanActivity : AppCompatActivity(), GrafikLahanView.MainView {

    private lateinit var presenter: GrafikLahanPresenter
    private var currentTypeLuas: String = "provinsi"
    private var currentTypeJumlah: String = "provinsi"
    private var stackValuesLuas = mutableListOf<List<EntryGrafikLahan>>()
    private var stackValuesJumlah = mutableListOf<List<EntryGrafikLahan>>()

    private val provinsi = mutableMapOf<String, DataLahan>()
    private val kabupaten = mutableMapOf<String, Map<String, DataLahan>>()
    private val kecamatan = mutableMapOf<String, Map<String, DataLahan>>()
    private val kelurahan = mutableMapOf<String, Map<String, DataLahan>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grafik_lahan)
        presenter = GrafikLahanPresenter(this, this)
        setupViews()
        onStartProgress()
        presenter.fetchLuasJumlahLahan()
    }

    private fun setupViews(){
        navigation_back.setOnClickListener { finish() }
        barchart_luas_lahan.setFitBars(true)
        barchart_luas_lahan.setDrawBarShadow(false)
        barchart_luas_lahan.setDrawValueAboveBar(true)
        barchart_luas_lahan.setDrawGridBackground(false)
        barchart_luas_lahan.axisLeft.setDrawGridLines(true)
        barchart_luas_lahan.axisRight.isEnabled = false
        barchart_luas_lahan.xAxis.setDrawGridLines(false)
        barchart_luas_lahan.xAxis.position = XAxis.XAxisPosition.BOTTOM
        barchart_luas_lahan.description.isEnabled = false
        barchart_luas_lahan.setPinchZoom(false)
        barchart_luas_lahan.legend.xOffset = -1000f
        barchart_luas_lahan.animateY(2500)
        barchart_luas_lahan.setXAxisRenderer(
            CustomXAxisRenderer(
                barchart_luas_lahan.viewPortHandler,
                barchart_luas_lahan.xAxis, barchart_luas_lahan.getTransformer(YAxis.AxisDependency.LEFT)
            )
        )

        barchart_jumlah_lahan.setFitBars(true)
        barchart_jumlah_lahan.setDrawBarShadow(false)
        barchart_jumlah_lahan.setDrawValueAboveBar(true)
        barchart_jumlah_lahan.setDrawGridBackground(false)
        barchart_jumlah_lahan.axisLeft.setDrawGridLines(true)
        barchart_jumlah_lahan.axisRight.isEnabled = false
        barchart_jumlah_lahan.xAxis.setDrawGridLines(false)
        barchart_jumlah_lahan.xAxis.position = XAxis.XAxisPosition.BOTTOM
        barchart_jumlah_lahan.description.isEnabled = false
        barchart_jumlah_lahan.setPinchZoom(false)
        barchart_jumlah_lahan.legend.xOffset = -1000f
        barchart_jumlah_lahan.animateY(1500)
        barchart_jumlah_lahan.setXAxisRenderer(
            CustomXAxisRenderer(
                barchart_jumlah_lahan.viewPortHandler,
                barchart_jumlah_lahan.xAxis, barchart_jumlah_lahan.getTransformer(YAxis.AxisDependency.LEFT)
            )
        )

        btn_prev_luas.setOnClickListener {
            predictNavigateLuas()
        }

        btn_prev_jumlah.setOnClickListener {
            predictNavigateJumlah()
        }
    }

    private fun predictNavigateLuas() {
        when(currentTypeLuas) {
            "provinsi"   -> { }
            "kabupaten"  -> {
                barchart_luas_lahan.data = BarData(
                    BarDataSet(getBarSetLuas(), "Provinsi").apply {
                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                    }
                )
                tv_distric_luas.text = "Provinsi"
                barchart_luas_lahan.xAxis.apply {
                    labelCount = getBarSetLuas().size
                    valueFormatter =
                        MyValueFormater(
                            getProvinsiName()
                        )
                }
                barchart_luas_lahan.data.notifyDataChanged()
                barchart_luas_lahan.notifyDataSetChanged()
                barchart_luas_lahan.invalidate()
                currentTypeLuas = "provinsi"
                btn_prev_luas.visibility = View.INVISIBLE
            }
            "kecamatan"  -> {
                val listEntryLuas = mutableListOf<BarEntry>().apply {
                    for (entry in stackValuesLuas[0]) {
                        add(entry.entryBar)
                    }
                }
                barchart_luas_lahan.data = BarData(
                    BarDataSet(listEntryLuas, stackValuesLuas[0][0].valueDataLahan.provinsi).apply {
                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                    }
                )
                tv_distric_luas.text = stackValuesLuas[0][0].valueDataLahan.provinsi
                barchart_luas_lahan.xAxis.apply {
                    labelCount = stackValuesLuas[0].size
                    val labels = mutableListOf<String>()
                    for (entry in stackValuesLuas[0]) {
                        labels.add(entry.valueDataLahan.kabupaten!!.toUpperCase().replace("KABUPATEN", "").trim().replace(" ", "\n").trim())
                    }
                    valueFormatter =
                        MyValueFormater(labels)
                }
                barchart_luas_lahan.data.notifyDataChanged()
                barchart_luas_lahan.notifyDataSetChanged()
                barchart_luas_lahan.invalidate()
                currentTypeLuas ="kabupaten"
            }
            "kelurahan"  -> {
                val listEntryLuas = mutableListOf<BarEntry>().apply {
                    for (entry in stackValuesLuas[1]) {
                        add(entry.entryBar)
                    }
                }
                barchart_luas_lahan.data = BarData(
                    BarDataSet(listEntryLuas, stackValuesLuas[1][0].valueDataLahan.kabupaten).apply {
                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                    }
                )
                tv_distric_luas.text = stackValuesLuas[1][0].valueDataLahan.kabupaten
                barchart_luas_lahan.xAxis.apply {
                    labelCount = stackValuesLuas[1].size
                    val labels = mutableListOf<String>()
                    for (entry in stackValuesLuas[1]) {
                        labels.add(entry.valueDataLahan.kecamatan!!.apply {
                            replace(" ", "\n")
                            trim()
                        })
                    }
                    valueFormatter =
                        MyValueFormater(labels)
                }
                barchart_luas_lahan.data.notifyDataChanged()
                barchart_luas_lahan.notifyDataSetChanged()
                barchart_luas_lahan.invalidate()
                currentTypeLuas = "kecamatan"
            }
        }
        barchart_luas_lahan.onTouchListener.setLastHighlighted(null)
        barchart_luas_lahan.highlightValue(null)
    }

    private fun predictNavigateJumlah() {
        when(currentTypeJumlah) {
            "provinsi"   -> { }
            "kabupaten"  -> {
                barchart_jumlah_lahan.data = BarData(
                    BarDataSet(getBarSetJumlah(), "Provinsi").apply {
                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                    }
                )
                tv_distric_jumlah.text = "Provinsi"
                barchart_jumlah_lahan.xAxis.apply {
                    labelCount = getBarSetJumlah().size
                    valueFormatter =
                        MyValueFormater(
                            getProvinsiName()
                        )
                }
                barchart_jumlah_lahan.data.notifyDataChanged()
                barchart_jumlah_lahan.notifyDataSetChanged()
                barchart_jumlah_lahan.invalidate()
                currentTypeJumlah = "provinsi"
                btn_prev_jumlah.visibility = View.INVISIBLE
            }
            "kecamatan"  -> {
                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                    for (entry in stackValuesJumlah[0]) {
                        add(entry.entryBar)
                    }
                }
                barchart_jumlah_lahan.data = BarData(
                    BarDataSet(listEntryJumlah, stackValuesJumlah[0][0].valueDataLahan.provinsi).apply {
                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                    }
                )
                tv_distric_jumlah.text = stackValuesJumlah[0][0].valueDataLahan.provinsi
                barchart_jumlah_lahan.xAxis.apply {
                    labelCount = stackValuesJumlah[0].size
                    val labels = mutableListOf<String>()
                    for (entry in stackValuesJumlah[0]) {
                        labels.add(entry.valueDataLahan.kabupaten!!.toUpperCase().replace("KABUPATEN", "").trim().replace(" ", "\n").trim())
                    }
                    valueFormatter =
                        MyValueFormater(labels)
                }
                barchart_jumlah_lahan.data.notifyDataChanged()
                barchart_jumlah_lahan.notifyDataSetChanged()
                barchart_jumlah_lahan.invalidate()
                currentTypeJumlah ="kabupaten"
            }
            "kelurahan"  -> {
                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                    for (entry in stackValuesJumlah[1]) {
                        add(entry.entryBar)
                    }
                }
                barchart_jumlah_lahan.data = BarData(
                    BarDataSet(listEntryJumlah, stackValuesJumlah[1][0].valueDataLahan.kabupaten).apply {
                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                    }
                )
                tv_distric_jumlah.text = stackValuesJumlah[1][0].valueDataLahan.kabupaten
                barchart_jumlah_lahan.xAxis.apply {
                    labelCount = stackValuesJumlah[1].size
                    val labels = mutableListOf<String>()
                    for (entry in stackValuesJumlah[1]) {
                        labels.add(entry.valueDataLahan.kecamatan!!.apply {
                            replace(" ", "\n")
                            trim()
                        })
                    }
                    valueFormatter =
                        MyValueFormater(labels)
                }
                barchart_jumlah_lahan.data.notifyDataChanged()
                barchart_jumlah_lahan.notifyDataSetChanged()
                barchart_jumlah_lahan.invalidate()
                currentTypeJumlah = "kecamatan"
            }
        }
        barchart_jumlah_lahan.onTouchListener.setLastHighlighted(null)
        barchart_jumlah_lahan.highlightValue(null)
    }

    private fun getBarSetLuas(): List<BarEntry> {
        val listBarSetLuas = mutableListOf<BarEntry>()
        for (p in provinsi.keys.withIndex()) {
            listBarSetLuas.add(BarEntry(p.index.toFloat(), provinsi[p.value]!!.luas_lahan_provinsi!!.toInt().toFloat()))
        }
        return listBarSetLuas
    }

    private fun getBarSetJumlah(): List<BarEntry> {
        val listBarSetJumlah = mutableListOf<BarEntry>()
        for (p in provinsi.keys.withIndex()) {
            listBarSetJumlah.add(BarEntry(p.index.toFloat(), provinsi[p.value]!!.jml_lahan_provinsi!!.toInt().toFloat()))
        }
        return listBarSetJumlah
    }

    private fun getProvinsiName(): List<String> {
        val listBarSetLuas = mutableListOf<String>()
        for (p in provinsi.keys.withIndex()) {
            listBarSetLuas.add(provinsi[p.value]!!.provinsi!!.replace(" ", "\n").trim())
        }
        return listBarSetLuas
    }

    private fun generateNextBarSetLuas(distric: String, data: Map<String, Map<String, DataLahan>>, districId: String): List<EntryGrafikLahan> {
        val nextBarSets = mutableListOf<EntryGrafikLahan>()
        for (d in (data[districId] ?: error("")).keys.withIndex()) {
            nextBarSets.add(
                EntryGrafikLahan(
                    d.value,
                    data[districId]!![d.value]!!,
                    BarEntry(
                        d.index.toFloat(),
                        data[districId]!![d.value]!!.let {
                            when(distric) {
                                "kabupaten"  -> {
                                    it.luas_lahan_kabupaten!!.toInt().toFloat()
                                }
                                "kecamatan"  -> {
                                    it.luas_lahan_kecamatan!!.toInt().toFloat()
                                }
                                "kelurahan"  -> {
                                    it.luas_lahan_kelurahan!!.toInt().toFloat()
                                }else -> 0f
                            }
                        }
                    )
                )
            )
        }
        return nextBarSets
    }

    private fun generateNextBarSetJumlah(distric: String, data: Map<String, Map<String, DataLahan>>, districId: String): List<EntryGrafikLahan> {
        val nextBarSets = mutableListOf<EntryGrafikLahan>()
        for (d in (data[districId] ?: error("")).keys.withIndex()) {
            nextBarSets.add(
                EntryGrafikLahan(
                    d.value,
                    data[districId]!![d.value]!!,
                    BarEntry(
                        d.index.toFloat(),
                        data[districId]!![d.value]!!.let {
                            when(distric) {
                                "kabupaten"  -> {
                                    it.jml_lahan_kabupaten!!.toInt().toFloat()
                                }
                                "kecamatan"  -> {
                                    it.jml_lahan_kecamatan!!.toInt().toFloat()
                                }
                                "kelurahan"  -> {
                                    it.jml_lahan_kelurahan!!.toInt().toFloat()
                                }else -> 0f
                            }
                        }
                    )
                )
            )
        }
        return nextBarSets
    }

    override fun onStartProgress() {
        progress_bar_luas.visibility = View.VISIBLE
        progress_bar_jumlah.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar_luas.visibility = View.GONE
        progress_bar_jumlah.visibility = View.GONE
    }

    override fun onLuasJumlahLahanFetched(listDataLahan: List<DataLahan>?) {
        onStopProgress()
        if (listDataLahan.isNullOrEmpty().not()) {

            for (data in listDataLahan!!) {
                if (provinsi.isNullOrEmpty()) {
                    provinsi[data.provinsi_id!!] = data
                }else{
                    if ((data.provinsi_id!! in provinsi.keys).not()) {
                        provinsi[data.provinsi_id] = data
                    }
                }

                data.kabupaten_id?.let {
                    if ((data.provinsi_id in kabupaten.keys).not()) {
                        kabupaten[data.provinsi_id] = mapOf(data.kabupaten_id!! to data)
                    }else {
                        kabupaten[data.provinsi_id] =
                            kabupaten[data.provinsi_id]!!.toMutableMap().apply {
                                if ((data.kabupaten_id!! in this).not()) {
                                    put(data.kabupaten_id, data)
                                }
                            }
                    }

                    data.kecamatan_id?.let {
                        if ((data.kabupaten_id in kecamatan.keys).not()) {
                            kecamatan[data.kabupaten_id!!] = mapOf(data.kecamatan_id!! to data)
                        }else {
                            kecamatan[data.kabupaten_id!!] = kecamatan[data.kabupaten_id]!!.toMutableMap().apply {
                                if ((data.kecamatan_id!! in this).not()) {
                                    put(data.kecamatan_id, data)
                                }
                            }
                        }

                        data.kelurahan_id?.let {
                            if ((data.kecamatan_id in kelurahan.keys).not()) {
                                kelurahan[data.kecamatan_id!!] = mapOf(data.kelurahan_id!! to data)
                            } else {
                                kelurahan[data.kecamatan_id!!] =
                                    kelurahan[data.kecamatan_id]!!.toMutableMap().apply {
                                        if ((data.kelurahan_id!! in this).not()) {
                                            put(data.kelurahan_id, data)
                                        }
                                    }
                            }
                        }
                    }
                }
            }

            barchart_luas_lahan.xAxis.apply {
                labelCount = getBarSetLuas().size
                valueFormatter =
                    MyValueFormater(
                        getProvinsiName()
                    )
                labelRotationAngle = 16f
            }
            barchart_luas_lahan.data = BarData(
                BarDataSet(getBarSetLuas(), "Provinsi").apply {
                    color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                }
            )
            barchart_luas_lahan.invalidate()
            tv_distric_luas.text = "Provinsi"

            barchart_jumlah_lahan.xAxis.apply {
                labelCount = getBarSetJumlah().size
                valueFormatter =
                    MyValueFormater(
                        getProvinsiName()
                    )
                labelRotationAngle = 16f
            }
            barchart_jumlah_lahan.data = BarData(
                BarDataSet(getBarSetJumlah(), "Provinsi").apply {
                    color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                }
            )
            barchart_jumlah_lahan.invalidate()
            tv_distric_jumlah.text = "Provinsi"

            barchart_luas_lahan.setOnChartValueSelectedListener(object: OnChartValueSelectedListener {
                override fun onNothingSelected() {}
                override fun onValueSelected(e: Entry?, h: Highlight?) {
                    e?.let {val position = e.x.toInt()
                        when(currentTypeLuas) {
                            "provinsi"   -> {
                                val entryLuas = generateNextBarSetLuas("kabupaten", kabupaten, provinsi.keys.toMutableList()[position])
                                val listEntryLuas = mutableListOf<BarEntry>().apply {
                                    for (entry in entryLuas) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_luas_lahan.data = BarData(
                                    BarDataSet(listEntryLuas,
                                        provinsi[provinsi.keys.toMutableList()[position]]!!.provinsi
                                        ).apply {
                                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_luas.text = provinsi[provinsi.keys.toMutableList()[position]]!!.provinsi
                                barchart_luas_lahan.xAxis.apply {
                                    labelCount = generateNextBarSetLuas("kabupaten", kabupaten, provinsi.keys.toMutableList()[position]).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryLuas) {
                                        labels.add(entry.valueDataLahan.kabupaten!!.toUpperCase().replace("KABUPATEN", "").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_luas_lahan.data.notifyDataChanged()
                                barchart_luas_lahan.notifyDataSetChanged()
                                barchart_luas_lahan.invalidate()
                                currentTypeLuas = "kabupaten"
                                if (stackValuesLuas.isNullOrEmpty() || stackValuesLuas.size == 0) {
                                    stackValuesLuas.add(0, generateNextBarSetLuas("kabupaten", kabupaten, provinsi.keys.toMutableList()[position]))
                                }else {
                                    stackValuesLuas[0] = generateNextBarSetLuas("kabupaten", kabupaten, provinsi.keys.toMutableList()[position])
                                }
                                btn_prev_luas.visibility = View.VISIBLE
                            }
                            "kabupaten"  -> {
                                val entryLuas = generateNextBarSetLuas("kecamatan", kecamatan, stackValuesLuas[0][position].valueId)
                                val listEntryLuas = mutableListOf<BarEntry>().apply {
                                    for (entry in entryLuas) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_luas_lahan.data = BarData(
                                    BarDataSet(listEntryLuas,
                                        kecamatan[stackValuesLuas[0][position].valueId]!!.values.toMutableList()[0].kabupaten
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_luas.text = kecamatan[stackValuesLuas[0][position].valueId]!!.values.toMutableList()[0].kabupaten
                                barchart_luas_lahan.xAxis.apply {
                                    labelCount = generateNextBarSetLuas("kecamatan", kecamatan, stackValuesLuas[0][position].valueId).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryLuas) {
                                        labels.add(entry.valueDataLahan.kecamatan!!. replace(" ", "\n").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_luas_lahan.data.notifyDataChanged()
                                barchart_luas_lahan.notifyDataSetChanged()
                                barchart_luas_lahan.invalidate()
                                currentTypeLuas = "kecamatan"
                                if (stackValuesLuas.size < 2) {
                                    stackValuesLuas.add(1, generateNextBarSetLuas("kecamatan", kecamatan, stackValuesLuas[0][position].valueId))
                                }else {
                                    stackValuesLuas[1] = generateNextBarSetLuas("kecamatan", kecamatan, stackValuesLuas[0][position].valueId)
                                }
                                btn_prev_luas.visibility = View.VISIBLE
                            }
                            "kecamatan"  -> {
                                val entryLuas = generateNextBarSetLuas("kelurahan", kelurahan, stackValuesLuas[1][position].valueId)
                                val listEntryLuas = mutableListOf<BarEntry>().apply {
                                    for (entry in entryLuas) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_luas_lahan.data = BarData(
                                    BarDataSet(listEntryLuas,
                                        kelurahan[stackValuesLuas[1][position].valueId]!!.values.toMutableList()[0].kecamatan
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_luas.text = kelurahan[stackValuesLuas[1][position].valueId]!!.values.toMutableList()[0].kecamatan
                                barchart_luas_lahan.xAxis.apply {
                                    labelCount = generateNextBarSetLuas("kelurahan", kelurahan, stackValuesLuas[1][position].valueId).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryLuas) {
                                        labels.add(entry.valueDataLahan.kelurahan!!.replace(" ", "\n").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_luas_lahan.data.notifyDataChanged()
                                barchart_luas_lahan.notifyDataSetChanged()
                                barchart_luas_lahan.invalidate()
                                currentTypeLuas = "kelurahan"
                                stackValuesLuas.add(2, generateNextBarSetLuas("kelurahan", kelurahan, stackValuesLuas[1][position].valueId))
                                btn_prev_luas.visibility = View.VISIBLE
                            }
                            "kelurahan"  -> { }
                        }
                    }
                    barchart_luas_lahan.onTouchListener.setLastHighlighted(null)
                    barchart_luas_lahan.highlightValue(null)
                }
            })

            barchart_jumlah_lahan.setOnChartValueSelectedListener(object: OnChartValueSelectedListener {
                override fun onNothingSelected() {}
                override fun onValueSelected(e: Entry?, h: Highlight?) {
                    e?.let {val position = e.x.toInt()
                        when(currentTypeJumlah) {
                            "provinsi"   -> {
                                val entryJumlah = generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position])
                                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                                    for (entry in entryJumlah) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_jumlah_lahan.data = BarData(
                                    BarDataSet(listEntryJumlah,
                                        provinsi[provinsi.keys.toMutableList()[position]]!!.provinsi
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_jumlah.text = provinsi[provinsi.keys.toMutableList()[position]]!!.provinsi
                                barchart_jumlah_lahan.xAxis.apply {
                                    labelCount = generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position]).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryJumlah) {
                                        labels.add(entry.valueDataLahan.kabupaten!!.toUpperCase().replace("KABUPATEN", "").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_jumlah_lahan.data.notifyDataChanged()
                                barchart_jumlah_lahan.notifyDataSetChanged()
                                barchart_jumlah_lahan.invalidate()
                                currentTypeJumlah = "kabupaten"
                                if (stackValuesJumlah.isNullOrEmpty() || stackValuesJumlah.size == 0) {
                                    stackValuesJumlah.add(0, generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position]))
                                }else {
                                    stackValuesJumlah[0] = generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position])
                                }
                                btn_prev_jumlah.visibility = View.VISIBLE
                            }
                            "kabupaten"  -> {
                                val entryJumlah = generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId)
                                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                                    for (entry in entryJumlah) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_jumlah_lahan.data = BarData(
                                    BarDataSet(listEntryJumlah,
                                        kecamatan[stackValuesJumlah[0][position].valueId]!!.values.toMutableList()[0].kabupaten
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_jumlah.text = kecamatan[stackValuesJumlah[0][position].valueId]!!.values.toMutableList()[0].kabupaten
                                barchart_jumlah_lahan.xAxis.apply {
                                    labelCount = generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryJumlah) {
                                        labels.add(entry.valueDataLahan.kecamatan!!. replace(" ", "\n").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_jumlah_lahan.data.notifyDataChanged()
                                barchart_jumlah_lahan.notifyDataSetChanged()
                                barchart_jumlah_lahan.invalidate()
                                currentTypeJumlah = "kecamatan"
                                if (stackValuesJumlah.size < 2) {
                                    stackValuesJumlah.add(1, generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId))
                                }else {
                                    stackValuesJumlah[1] = generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId)
                                }
                                btn_prev_jumlah.visibility = View.VISIBLE
                            }
                            "kecamatan"  -> {
                                val entryJumlah = generateNextBarSetJumlah("kelurahan", kelurahan, stackValuesJumlah[1][position].valueId)
                                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                                    for (entry in entryJumlah) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_jumlah_lahan.data = BarData(
                                    BarDataSet(listEntryJumlah,
                                        kelurahan[stackValuesJumlah[1][position].valueId]!!.values.toMutableList()[0].kecamatan
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikLahanActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_jumlah.text = kelurahan[stackValuesJumlah[1][position].valueId]!!.values.toMutableList()[0].kecamatan
                                barchart_jumlah_lahan.xAxis.apply {
                                    labelCount = generateNextBarSetJumlah("kelurahan", kelurahan, stackValuesJumlah[1][position].valueId).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryJumlah) {
                                        labels.add(entry.valueDataLahan.kelurahan!!.replace(" ", "\n").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_jumlah_lahan.data.notifyDataChanged()
                                barchart_jumlah_lahan.notifyDataSetChanged()
                                barchart_jumlah_lahan.invalidate()
                                currentTypeJumlah = "kelurahan"
                                stackValuesJumlah.add(2, generateNextBarSetJumlah("kelurahan", kelurahan, stackValuesJumlah[1][position].valueId))
                                btn_prev_jumlah.visibility = View.VISIBLE
                            }
                            "kelurahan"  -> { }
                        }
                    }
                    barchart_jumlah_lahan.onTouchListener.setLastHighlighted(null)
                    barchart_jumlah_lahan.highlightValue(null)
                }
            })
        }
        ll_legend_luas.visibility = View.VISIBLE
        ll_legend_jumlah.visibility = View.VISIBLE
    }

    override fun onFailedFetchLuasJumlahLahan(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi Internet Offline", Toast.LENGTH_SHORT).show()
    }
}