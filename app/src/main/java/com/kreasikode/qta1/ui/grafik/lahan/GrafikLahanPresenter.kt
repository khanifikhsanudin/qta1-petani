package com.kreasikode.qta1.ui.grafik.lahan

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class GrafikLahanPresenter(val context: Context, val mainView: GrafikLahanView.MainView): GrafikLahanView.MainPresenter {
    override fun fetchLuasJumlahLahan() {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_MASTER_LAHAN)
                .build().getAsObject(
                    GrafikLahanResponse::class.java,
                    object : ParsedRequestListener<GrafikLahanResponse> {
                        override fun onResponse(response: GrafikLahanResponse) {
                            if (response.meta?.code != null && response.meta.code == 200) {
                                if (response.meta.error == false) {
                                    mainView.onLuasJumlahLahanFetched(response.data)
                                }else {
                                    mainView.onFailedFetchLuasJumlahLahan(response.meta.message)
                                }
                            }else {
                                if (response.data != null) {
                                    mainView.onLuasJumlahLahanFetched(response.data)
                                }else {
                                    mainView.onFailedFetchLuasJumlahLahan("Error")
                                }
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedFetchLuasJumlahLahan(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}