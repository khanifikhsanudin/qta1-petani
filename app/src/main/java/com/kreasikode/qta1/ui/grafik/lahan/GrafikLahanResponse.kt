package com.kreasikode.qta1.ui.grafik.lahan

import com.kreasikode.qta1.model.DataLahan
import com.kreasikode.qta1.model.Meta

data class GrafikLahanResponse(
    val meta: Meta?,
    val data: List<DataLahan>?
)