package com.kreasikode.qta1.ui.grafik.lahan

import com.kreasikode.qta1.model.DataLahan

class GrafikLahanView {

    interface MainPresenter {
        fun fetchLuasJumlahLahan()
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onLuasJumlahLahanFetched(listDataLahan: List<DataLahan>?)
        fun onFailedFetchLuasJumlahLahan(message: String?)
        fun onOffline()
    }
}