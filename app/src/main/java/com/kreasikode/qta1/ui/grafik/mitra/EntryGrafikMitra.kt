package com.kreasikode.qta1.ui.grafik.mitra

import com.github.mikephil.charting.data.BarEntry
import com.kreasikode.qta1.model.DataMitraWilayah


data class EntryGrafikMitra(
    val valueId: String,
    val valueDataLahan: DataMitraWilayah,
    val entryBar: BarEntry
)