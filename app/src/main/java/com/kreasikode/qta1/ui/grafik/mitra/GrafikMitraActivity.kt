package com.kreasikode.qta1.ui.grafik.mitra

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.DataMitraWilayah
import com.kreasikode.qta1.ui.grafik.MyValueFormater
import com.kreasikode.qta1.widget.CustomXAxisRenderer
import kotlinx.android.synthetic.main.activity_grafik_mitra.*

class GrafikMitraActivity : AppCompatActivity(), GrafikMitraView.MainView {

    private lateinit var presenter: GrafikMitraPresenter
    private var currentTypeJumlah: String = "provinsi"
    private var stackValuesJumlah = mutableListOf<List<EntryGrafikMitra>>()

    private val provinsi = mutableMapOf<String, DataMitraWilayah>()
    private val kabupaten = mutableMapOf<String, Map<String, DataMitraWilayah>>()
    private val kecamatan = mutableMapOf<String, Map<String, DataMitraWilayah>>()
    private val kelurahan = mutableMapOf<String, Map<String, DataMitraWilayah>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grafik_mitra)
        presenter = GrafikMitraPresenter(this, this)
        setupViews()
        onStartProgress()
        presenter.fetchJumlahMitra()
    }

    private fun setupViews(){
        navigation_back.setOnClickListener { finish() }
        barchart_jumlah_mitra.setFitBars(true)
        barchart_jumlah_mitra.setDrawBarShadow(false)
        barchart_jumlah_mitra.setDrawValueAboveBar(true)
        barchart_jumlah_mitra.setDrawGridBackground(false)
        barchart_jumlah_mitra.axisLeft.setDrawGridLines(true)
        barchart_jumlah_mitra.axisRight.isEnabled = false
        barchart_jumlah_mitra.xAxis.setDrawGridLines(false)
        barchart_jumlah_mitra.xAxis.position = XAxis.XAxisPosition.BOTTOM
        barchart_jumlah_mitra.description.isEnabled = false
        barchart_jumlah_mitra.setPinchZoom(false)
        barchart_jumlah_mitra.legend.xOffset = -1000f
        barchart_jumlah_mitra.animateY(1500)
        barchart_jumlah_mitra.setXAxisRenderer(
            CustomXAxisRenderer(
                barchart_jumlah_mitra.viewPortHandler,
                barchart_jumlah_mitra.xAxis, barchart_jumlah_mitra.getTransformer(YAxis.AxisDependency.LEFT)
            )
        )

        btn_prev_jumlah.setOnClickListener {
            predictNavigateJumlah()
        }
    }

    private fun predictNavigateJumlah() {
        when(currentTypeJumlah) {
            "provinsi"   -> { }
            "kabupaten"  -> {
                barchart_jumlah_mitra.data = BarData(
                    BarDataSet(getBarSetJumlah(), "Provinsi").apply {
                        color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                    }
                )
                tv_distric_jumlah.text = "Provinsi"
                barchart_jumlah_mitra.xAxis.apply {
                    labelCount = getBarSetJumlah().size
                    valueFormatter =
                        MyValueFormater(
                            getProvinsiName()
                        )
                }
                barchart_jumlah_mitra.data.notifyDataChanged()
                barchart_jumlah_mitra.notifyDataSetChanged()
                barchart_jumlah_mitra.invalidate()
                currentTypeJumlah = "provinsi"
                btn_prev_jumlah.visibility = View.INVISIBLE
            }
            "kecamatan"  -> {
                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                    for (entry in stackValuesJumlah[0]) {
                        add(entry.entryBar)
                    }
                }
                barchart_jumlah_mitra.data = BarData(
                    BarDataSet(listEntryJumlah, stackValuesJumlah[0][0].valueDataLahan.provinsi).apply {
                        color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                    }
                )
                tv_distric_jumlah.text = stackValuesJumlah[0][0].valueDataLahan.provinsi
                barchart_jumlah_mitra.xAxis.apply {
                    labelCount = stackValuesJumlah[0].size
                    val labels = mutableListOf<String>()
                    for (entry in stackValuesJumlah[0]) {
                        labels.add(entry.valueDataLahan.kabupaten!!.toUpperCase().replace("KABUPATEN", "").trim().replace(" ", "\n").trim())
                    }
                    valueFormatter =
                        MyValueFormater(labels)
                }
                barchart_jumlah_mitra.data.notifyDataChanged()
                barchart_jumlah_mitra.notifyDataSetChanged()
                barchart_jumlah_mitra.invalidate()
                currentTypeJumlah ="kabupaten"
            }
            "kelurahan"  -> {
                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                    for (entry in stackValuesJumlah[1]) {
                        add(entry.entryBar)
                    }
                }
                barchart_jumlah_mitra.data = BarData(
                    BarDataSet(listEntryJumlah, stackValuesJumlah[1][0].valueDataLahan.kabupaten).apply {
                        color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                    }
                )
                tv_distric_jumlah.text = stackValuesJumlah[1][0].valueDataLahan.kabupaten
                barchart_jumlah_mitra.xAxis.apply {
                    labelCount = stackValuesJumlah[1].size
                    val labels = mutableListOf<String>()
                    for (entry in stackValuesJumlah[1]) {
                        labels.add(entry.valueDataLahan.kecamatan!!.apply {
                            replace(" ", "\n")
                            trim()
                        })
                    }
                    valueFormatter =
                        MyValueFormater(labels)
                }
                barchart_jumlah_mitra.data.notifyDataChanged()
                barchart_jumlah_mitra.notifyDataSetChanged()
                barchart_jumlah_mitra.invalidate()
                currentTypeJumlah = "kecamatan"
            }
        }
        barchart_jumlah_mitra.onTouchListener.setLastHighlighted(null)
        barchart_jumlah_mitra.highlightValue(null)
    }

    private fun getBarSetJumlah(): List<BarEntry> {
        val listBarSetJumlah = mutableListOf<BarEntry>()
        for (p in provinsi.keys.withIndex()) {
            listBarSetJumlah.add(BarEntry(p.index.toFloat(), provinsi[p.value]!!.jml_mitra_provinsi!!.toInt().toFloat()))
        }
        return listBarSetJumlah
    }

    private fun getProvinsiName(): List<String> {
        val listBarSetLuas = mutableListOf<String>()
        for (p in provinsi.keys.withIndex()) {
            listBarSetLuas.add(provinsi[p.value]!!.provinsi!!.replace(" ", "\n").trim())
        }
        return listBarSetLuas
    }

    private fun generateNextBarSetJumlah(distric: String, data: Map<String, Map<String, DataMitraWilayah>>, districId: String): List<EntryGrafikMitra> {
        val nextBarSets = mutableListOf<EntryGrafikMitra>()
        if (districId in data.keys) {
            for (d in (data[districId] ?: error("")).keys.withIndex()) {
                nextBarSets.add(
                    EntryGrafikMitra(
                        d.value,
                        data[districId]!![d.value]!!,
                        BarEntry(
                            d.index.toFloat(),
                            data[districId]!![d.value]!!.let {
                                when(distric) {
                                    "kabupaten"  -> {
                                        it.jml_mitra_kabupaten!!.toInt().toFloat()
                                    }
                                    "kecamatan"  -> {
                                        it.jml_mitra_kecamatan!!.toInt().toFloat()
                                    }
                                    "kelurahan"  -> {
                                        it.jml_mitra_kelurahan!!.toInt().toFloat()
                                    }else -> 0f
                                }
                            }
                        )
                    )
                )
            }
        }
        return nextBarSets
    }

    override fun onStartProgress() {
        progress_bar_jumlah.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar_jumlah.visibility = View.GONE
    }

    override fun onJumlahMitraFetched(listDataMitraWilayah: List<DataMitraWilayah>?) {
        onStopProgress()
        if (listDataMitraWilayah.isNullOrEmpty().not()) {
            var lolos = true
            for (data in listDataMitraWilayah!!) {

                if (provinsi.isNullOrEmpty()) {
                    provinsi[data.provinsi_id!!] = data
                } else {
                    if ((data.provinsi_id!! in provinsi.keys).not()) {
                        provinsi[data.provinsi_id] = data
                    }
                }

                if ((data.provinsi_id in kabupaten.keys).not()) {
                    kabupaten[data.provinsi_id] = mapOf(data.kabupaten_id!! to data)
                } else {
                    kabupaten[data.provinsi_id] =
                        kabupaten[data.provinsi_id]!!.toMutableMap().apply {
                            if ((data.kabupaten_id!! in this).not()) {
                                put(data.kabupaten_id, data)
                            }
                        }
                }

                if ((data.kabupaten_id in kecamatan.keys).not()) {
                    kecamatan[data.kabupaten_id!!] = mapOf(data.kecamatan_id!! to data)
                } else {
                    kecamatan[data.kabupaten_id!!] =
                        kecamatan[data.kabupaten_id]!!.toMutableMap().apply {
                            if ((data.kecamatan_id!! in this).not()) {
                                put(data.kecamatan_id, data)
                            }
                        }
                }

                data.kelurahan_id?.let {
                    if ((data.kecamatan_id in kelurahan.keys).not()) {
                        kelurahan[data.kecamatan_id!!] = mapOf(data.kelurahan_id!! to data)
                    } else {
                        kelurahan[data.kecamatan_id!!] =
                            kelurahan[data.kecamatan_id]!!.toMutableMap().apply {
                                if ((data.kelurahan_id!! in this).not()) {
                                    put(data.kelurahan_id, data)
                                }
                            }
                    }
                }

//                if (data.provinsi_id == null ||
//                    data.provinsi == null ||
//                    data.jml_mitra_provinsi == null ||
//                    data.kabupaten_id == null ||
//                    data.kabupaten == null ||
//                    data.jml_mitra_kabupaten == null ||
//                    data.kecamatan_id == null ||
//                    data.kecamatan == null ||
//                    data.jml_mitra_kecamatan == null ||
//                    data.kelurahan_id == null ||
//                    data.kelurahan == null ||
//                    data.jml_mitra_kelurahan == null) {
//                    lolos = false
//                }else {
//
//
//                }
            }

            barchart_jumlah_mitra.xAxis.apply {
                labelCount = getBarSetJumlah().size
                valueFormatter = MyValueFormater(getProvinsiName())
                labelRotationAngle = 16f
            }
            barchart_jumlah_mitra.data = BarData(
                BarDataSet(getBarSetJumlah(), "Provinsi").apply {
                    color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                }
            )
            barchart_jumlah_mitra.invalidate()
            tv_distric_jumlah.text = "Provinsi"

            barchart_jumlah_mitra.setOnChartValueSelectedListener(object:
                OnChartValueSelectedListener {
                override fun onNothingSelected() {}
                override fun onValueSelected(e: Entry?, h: Highlight?) {
                    e?.let {val position = e.x.toInt()
                        when(currentTypeJumlah) {
                            "provinsi"   -> {
                                val entryJumlah = generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position])
                                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                                    for (entry in entryJumlah) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_jumlah_mitra.data = BarData(
                                    BarDataSet(listEntryJumlah,
                                        provinsi[provinsi.keys.toMutableList()[position]]!!.provinsi
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_jumlah.text = provinsi[provinsi.keys.toMutableList()[position]]!!.provinsi
                                barchart_jumlah_mitra.xAxis.apply {
                                    labelCount = generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position]).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryJumlah) {
                                        labels.add(entry.valueDataLahan.kabupaten!!.toUpperCase().replace("KABUPATEN", "").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_jumlah_mitra.data.notifyDataChanged()
                                barchart_jumlah_mitra.notifyDataSetChanged()
                                barchart_jumlah_mitra.invalidate()
                                currentTypeJumlah = "kabupaten"
                                if (stackValuesJumlah.isNullOrEmpty() || stackValuesJumlah.size == 0) {
                                    stackValuesJumlah.add(0, generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position]))
                                }else {
                                    stackValuesJumlah[0] = generateNextBarSetJumlah("kabupaten", kabupaten, provinsi.keys.toMutableList()[position])
                                }
                                btn_prev_jumlah.visibility = View.VISIBLE
                            }
                            "kabupaten"  -> {
                                val entryJumlah = generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId)
                                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                                    for (entry in entryJumlah) {
                                        add(entry.entryBar)
                                    }
                                }
                                barchart_jumlah_mitra.data = BarData(
                                    BarDataSet(listEntryJumlah,
                                        kecamatan[stackValuesJumlah[0][position].valueId]!!.values.toMutableList()[0].kabupaten
                                    ).apply {
                                        color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                                    }
                                )
                                tv_distric_jumlah.text = kecamatan[stackValuesJumlah[0][position].valueId]!!.values.toMutableList()[0].kabupaten
                                barchart_jumlah_mitra.xAxis.apply {
                                    labelCount = generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId).size
                                    val labels = mutableListOf<String>()
                                    for (entry in entryJumlah) {
                                        labels.add(entry.valueDataLahan.kecamatan!!. replace(" ", "\n").trim())
                                    }
                                    valueFormatter =
                                        MyValueFormater(
                                            labels
                                        )
                                }
                                barchart_jumlah_mitra.data.notifyDataChanged()
                                barchart_jumlah_mitra.notifyDataSetChanged()
                                barchart_jumlah_mitra.invalidate()
                                currentTypeJumlah = "kecamatan"
                                if (stackValuesJumlah.size < 2) {
                                    stackValuesJumlah.add(1, generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId))
                                }else {
                                    stackValuesJumlah[1] = generateNextBarSetJumlah("kecamatan", kecamatan, stackValuesJumlah[0][position].valueId)
                                }
                                btn_prev_jumlah.visibility = View.VISIBLE
                            }
                            "kecamatan"  -> {
                                val entryJumlah = generateNextBarSetJumlah("kelurahan", kelurahan, stackValuesJumlah[1][position].valueId)
                                val listEntryJumlah = mutableListOf<BarEntry>().apply {
                                    for (entry in entryJumlah) {
                                        add(entry.entryBar)
                                    }
                                }

                                if(!entryJumlah.isNullOrEmpty()) {
                                    barchart_jumlah_mitra.data = BarData(
                                        BarDataSet(listEntryJumlah,
                                            kelurahan[stackValuesJumlah[1][position].valueId]!!.values.toMutableList()[0].kecamatan
                                        ).apply {
                                            color = ContextCompat.getColor(this@GrafikMitraActivity, R.color.colorAccent)
                                        }
                                    )
                                    tv_distric_jumlah.text = kelurahan[stackValuesJumlah[1][position].valueId]!!.values.toMutableList()[0].kecamatan
                                    barchart_jumlah_mitra.xAxis.apply {
                                        labelCount = generateNextBarSetJumlah("kelurahan", kelurahan, stackValuesJumlah[1][position].valueId).size
                                        val labels = mutableListOf<String>()
                                        for (entry in entryJumlah) {
                                            labels.add(entry.valueDataLahan.kelurahan!!.replace(" ", "\n").trim())
                                        }
                                        valueFormatter =
                                            MyValueFormater(
                                                labels
                                            )
                                    }
                                    barchart_jumlah_mitra.data.notifyDataChanged()
                                    barchart_jumlah_mitra.notifyDataSetChanged()
                                    barchart_jumlah_mitra.invalidate()
                                    currentTypeJumlah = "kelurahan"
                                    stackValuesJumlah.add(2, generateNextBarSetJumlah("kelurahan", kelurahan, stackValuesJumlah[1][position].valueId))
                                    btn_prev_jumlah.visibility = View.VISIBLE
                                }else {
                                    Toast.makeText(this@GrafikMitraActivity, "Data belum tersedia", Toast.LENGTH_SHORT).show()
                                }
                            }
                            "kelurahan"  -> { }
                        }
                    }
                    barchart_jumlah_mitra.onTouchListener.setLastHighlighted(null)
                    barchart_jumlah_mitra.highlightValue(null)
                }
            })

            if (lolos.not()) {
                Toast.makeText(this, "Data tidak lengkap. Terdapat beberapa data yang null.", Toast.LENGTH_LONG).show()
            }
        }
        ll_legend_jumlah.visibility = View.VISIBLE
    }

    override fun onFailedFetchJumlahMitra(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi Internet Offline", Toast.LENGTH_SHORT).show()
    }
}
