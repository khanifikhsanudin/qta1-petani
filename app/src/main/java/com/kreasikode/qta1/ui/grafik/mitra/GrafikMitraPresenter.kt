package com.kreasikode.qta1.ui.grafik.mitra

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class GrafikMitraPresenter(val context: Context, val mainView: GrafikMitraView.MainView): GrafikMitraView.MainPresenter {

    override fun fetchJumlahMitra() {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_MASTER_MITRA)
                .build().getAsObject(
                    GrafikMitraResponse::class.java,
                    object : ParsedRequestListener<GrafikMitraResponse> {
                        override fun onResponse(response: GrafikMitraResponse) {
                            if (response.meta?.code != null && response.meta.code == 200) {
                                if (response.meta.error == false) {
                                    mainView.onJumlahMitraFetched(response.data)
                                }else {
                                    mainView.onFailedFetchJumlahMitra(response.meta.message)
                                }
                            }else {
                                if (response.data != null) {
                                    mainView.onJumlahMitraFetched(response.data)
                                }else {
                                    mainView.onFailedFetchJumlahMitra("Error")
                                }
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedFetchJumlahMitra(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}