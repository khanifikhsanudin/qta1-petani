package com.kreasikode.qta1.ui.grafik.mitra

import com.kreasikode.qta1.model.DataMitraWilayah
import com.kreasikode.qta1.model.Meta

data class GrafikMitraResponse(
    val meta: Meta?,
    val data: List<DataMitraWilayah>?
)