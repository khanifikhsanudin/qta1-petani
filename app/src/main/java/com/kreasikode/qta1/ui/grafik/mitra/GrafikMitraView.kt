package com.kreasikode.qta1.ui.grafik.mitra

import com.kreasikode.qta1.model.DataMitraWilayah

class GrafikMitraView {

    interface MainPresenter {
        fun fetchJumlahMitra()
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onJumlahMitraFetched(listDataMitraWilayah: List<DataMitraWilayah>?)
        fun onFailedFetchJumlahMitra(message: String?)
        fun onOffline()
    }
}