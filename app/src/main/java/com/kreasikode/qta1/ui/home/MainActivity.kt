package com.kreasikode.qta1.ui.home

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import ca.mimic.oauth2library.OAuth2Client
import co.komiku.komiku.adapter.SliderAdapter
import com.google.gson.Gson
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.CompanyHomeAdapter
import com.kreasikode.qta1.model.CompanyFarm
import com.kreasikode.qta1.model.MasterToken
import com.kreasikode.qta1.model.Member
import com.kreasikode.qta1.ui.aksespasar.AksesPasarActivity
import com.kreasikode.qta1.ui.artikel.ArtikelActivity
import com.kreasikode.qta1.ui.detail.member.DetailMemberActivity
import com.kreasikode.qta1.ui.detail.member.DetailMemberPresenter
import com.kreasikode.qta1.ui.detail.member.DetailMemberView
import com.kreasikode.qta1.ui.grafik.lahan.GrafikLahanActivity
import com.kreasikode.qta1.ui.grafik.mitra.GrafikMitraActivity
import com.kreasikode.qta1.ui.monitoring.dashboard.DashboardMonitoringActivity
import com.kreasikode.qta1.ui.tentang.TentangActivity
import com.kreasikode.qta1.ui.toko.TokoActivity
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.PreferencesManager
import com.kreasikode.qta1.utils.Utility
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import kotlinx.android.synthetic.main.activity_main.*
import net.openid.appauth.*


class MainActivity : AppCompatActivity(), DetailMemberView.MainView {

    private lateinit var sliderAdapter: SliderAdapter
    private lateinit var companyHomeAdapter: CompanyHomeAdapter
    private lateinit var mAuthService: AuthorizationService
    private lateinit var memberPresenter: DetailMemberPresenter
    private lateinit var loading: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//
//        val resp = AuthorizationResponse.fromIntent(intent)
//        val ex = AuthorizationException.fromIntent(intent)
//        resp?.let {
//            Toast.makeText(this, "good : " + it.accessToken?:"null", Toast.LENGTH_LONG).show()
//        }
//        ex?.let {
//            Toast.makeText(this, "fail : " + it.errorDescription?:"null", Toast.LENGTH_LONG).show()
//        }

        mAuthService =  AuthorizationService(this)
        memberPresenter = DetailMemberPresenter(this, this)
        loading = Utility.setProgressDialog(this, "Memuat...")

        ll_auth.setOnClickListener {
            doAuth()
//            startActivityForResult(Intent(this, AuthActivity::class.java), AUTH_CODE)
        }
        setupMember()

        companyHomeAdapter = CompanyHomeAdapter {
//            startActivity(DetailAksesPasarActivity.createIntent(this, it))
        }
//        rv_investor.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//        rv_investor.adapter = companyHomeAdapter

        sliderAdapter = SliderAdapter()
        slider.setIndicatorAnimation(IndicatorAnimations.WORM)
        slider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        slider.sliderAdapter = sliderAdapter
        val  listImg = mutableListOf<Int>(
            R.drawable.slide0,
            R.drawable.slide1,
            R.drawable.slide2,
            R.drawable.slide3,
            R.drawable.slide4,
            R.drawable.slide5
        )
        sliderAdapter.setData(listImg)
        slider.startAutoCycle()

        ImageUtil.fromDrawable(this.applicationContext, ContextCompat.getDrawable(this, R.drawable.logo)!!, iv_logo)
        menu_1.setOnClickListener { startActivity(Intent(this, TentangActivity::class.java)) }
        menu_2.setOnClickListener { startActivity(Intent(this, ArtikelActivity::class.java)) }
        menu_3.setOnClickListener { startActivity(Intent(this, TokoActivity::class.java)) }
        menu_4.setOnClickListener { startActivity(Intent(this, AksesPasarActivity::class.java)) }
        menu_5.setOnClickListener { startActivity(Intent(this, GrafikLahanActivity::class.java)) }
        menu_6.setOnClickListener { startActivity(Intent(this, GrafikMitraActivity::class.java)) }

        menu_feature1.setOnClickListener { startActivity(Intent(this, DashboardMonitoringActivity::class.java)) }

        ImageUtil.fromUrl(this.applicationContext,"https://www.static-src.com/wcsstore/Indraprastha/images/catalog/mlogo/FOS-35367.png",iv_food)
        ImageUtil.fromUrl(this.applicationContext,"http://www.bulog.co.id/images/logoperumbulog.jpg",iv_bulog)
        ImageUtil.fromDrawable(this.applicationContext,resources.getDrawable(R.drawable.kpsm),iv_kpsm)

        val listInvestor = mutableListOf<CompanyFarm>()
        listInvestor.add(
            CompanyFarm(
            "Bayu Aji",
            "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
            "087715161160",
            "Bulog divre katsuro", R.drawable.img_bulog,
                "Bulog divre kartosuro adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
        ))

        listInvestor.add(CompanyFarm(
            "Karunia Arif",
            "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
            "087715161160",
            "Bulog divre A", R.drawable.img_bulog,
            "Bulog divre A adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
        ))

        listInvestor.add(CompanyFarm(
            "Dwiana Ardi",
            "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
            "087715161160",
            "Bulog divre B", R.drawable.img_bulog,
            "Bulog divre B adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
        ))

        listInvestor.add(CompanyFarm(
            "Galih Aprianto",
            "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
            "087715161160",
            "Bulog divre C", R.drawable.img_bulog,
            "Bulog divre C adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
        ))

        listInvestor.add(CompanyFarm(
            "Andri Saefudin",
            "Jalan Garuda No 78A Daerah Istimewa Yogyakarta",
            "087715161160",
            "Bulog divre D", R.drawable.img_bulog,
            "Bulog divre D adalah pengepul terbaik yang akan membeli produk produk yang anda jual dengan harga yang tinggi dan memiliki layanan tercepet diantara bulog bulog lainnya"
        ))

        companyHomeAdapter.setData(listInvestor)

        val ssoClient = OAuth2Client.Builder("sadmin", "sadmin", "qta1-mobile", "qta1-mobile", "https://sso.petani1.com/sso/token")
            .grantType("password").build()

//        ssoClient.requestAccessToken { response ->
//            response?.let {
//                if (response.isSuccessful) {
//                    runOnUiThread {
//                        Toast.makeText(this@MainActivity, it.accessToken, Toast.LENGTH_LONG).show()
//                    }
//                } else {
//                    runOnUiThread {
//                        Toast.makeText(this@MainActivity, "error: " + it.body?:"null", Toast.LENGTH_LONG).show()
//                    }
//                }
//            }
//        }

    }

    private fun doAuth() {
        val serviceConfig = AuthorizationServiceConfiguration(
            Uri.parse("https://sso.qta1.com/sso/authorize"),
            Uri.parse("https://sso.qta1.com/sso/token"))

        val authBuilder = AuthorizationRequest.Builder(
            serviceConfig,
            "qta1-mobile", ResponseTypeValues.CODE, Uri.parse("com.pingidentity.developer.appauth://oidc_callback")
        ).build()

        val intent = mAuthService.getAuthorizationRequestIntent(authBuilder)
        startActivityForResult(intent, 5000)
    }

    private fun getShortDisplayName(name: String): String? {
        val maxChar = 12
        val splited = name.split("\\s+").toTypedArray()
        val shortName = StringBuilder()
        if (splited[0].length >= maxChar) {
            shortName.append(name, 0, maxChar)
        } else {
            for (s in splited) {
                if (shortName.toString().length + s.length <= maxChar) {
                    shortName.append(" ").append(s)
                } else {
                    break
                }
            }
        }
        return shortName.toString()
    }

    private fun setupMember() {
        val prefs = PreferencesManager.init(this)
        if (prefs.masterToken != null) {
            tv_member_name.text = if (prefs.memberData != null) getShortDisplayName(prefs.memberData?.nama_lengkap_qta1?:"Anonim") else "Profile"
            ll_auth.setOnClickListener {
                startActivityForResult(Intent(this, DetailMemberActivity::class.java), AUTH_CODE)
            }
            Handler().postDelayed({
                ll_member.visibility = View.VISIBLE
            }, 800)
            ll_member.visibility = View.VISIBLE
        }else {
            tv_member_name.text = "Login"
            ll_auth.setOnClickListener { doAuth() }
            ll_member.visibility = View.GONE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTH_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                setupMember()
            }
        }
        if (requestCode == 5000) {
            val resp = AuthorizationResponse.fromIntent(data!!)
            val ex = AuthorizationException.fromIntent(data)
            resp?.let {
//                Toast.makeText(this, "Good : " + Gson().toJson(it)?:"null", Toast.LENGTH_LONG).show()
                mAuthService.performTokenRequest(it.createTokenExchangeRequest(), ClientSecretBasic("qta1-mobile")) { response, ex ->
                    if (response != null) {
//                        Toast.makeText(this@MainActivity, "Token : " + Gson().toJson(response), Toast.LENGTH_LONG).show()
                        PreferencesManager.init(this).masterToken = MasterToken(
                            response.accessToken,
                            response.accessTokenExpirationTime,
                            response.refreshToken,
                            response.request.authorizationCode,
                            response.request.clientId,
                            response.request.codeVerifier,
                            response.request.grantType,
                            response.scope,
                            response.tokenType
                        )
                        onStartProgress()
                        memberPresenter.fetchUserDetail(response.accessToken?:"")
                    }else {
                        ex?.let {
                            Toast.makeText(this@MainActivity, "Fail Get Token : " + Gson().toJson(it), Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
            ex?.let {
//                Toast.makeText(this, "Fail : " + it.toJsonString()?:"null", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStartProgress() {
        loading.show()
    }

    override fun onStopProgress() {
        loading.hide()
    }

    override fun onUserDetailFetched(member: Member) {
        onStopProgress()
        PreferencesManager.init(this).memberData = member
        setupMember()
    }

    override fun onFailed(message: String?) {
        onStopProgress()
        setupMember()
    }

    override fun onOffline() {
        onStopProgress()
        setupMember()
    }

    companion object {
        const val AUTH_CODE = 1005
    }
}
