package com.kreasikode.qta1.ui.monitoring.budidaya

import com.kreasikode.qta1.model.DataBodySpk

data class GetBodySpkResponse(
    val data: List<DataBodySpk>?
)