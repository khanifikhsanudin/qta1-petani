package com.kreasikode.qta1.ui.monitoring.budidaya

import com.kreasikode.qta1.model.DataInvestorSpk

data class GetInvestorSpkResponse(
    val data: List<DataInvestorSpk>?
)