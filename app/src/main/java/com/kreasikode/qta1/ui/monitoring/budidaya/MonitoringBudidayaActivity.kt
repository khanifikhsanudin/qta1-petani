package com.kreasikode.qta1.ui.monitoring.budidaya

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.BudidayaAdapter
import com.kreasikode.qta1.model.DataBodySpk
import com.kreasikode.qta1.model.DataInvestorSpk
import com.kreasikode.qta1.ui.monitoring.laporan.LaporanKeuanganActivity
import com.kreasikode.qta1.utils.PreferencesManager
import com.kreasikode.qta1.utils.Utility
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import kotlinx.android.synthetic.main.activity_monitoring_budidaya.*

class MonitoringBudidayaActivity : AppCompatActivity(), MonitoringBudidayaView.MainView {

    private lateinit var presenter: MonitoringBudidayaView.MainPresenter
    private lateinit var budidayaAdapter: BudidayaAdapter

    private val mListDataInvestorSpk = mutableListOf<DataInvestorSpk>()
    private val mListDataBodySpk = mutableListOf<DataBodySpk>()

    private var currentSpkPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monitoring_budidaya)
        presenter = MonitoringBudidayaPresenter(this, this)

        setupView()
    }

    private fun setupView(){
        val prefs = PreferencesManager.init(this)
        val investorId = prefs.memberData?.user_id_qta1
        val role = prefs.memberData?.role

        navigation_back.setOnClickListener { finish() ; Utility.finishTransition(this) }

        budidayaAdapter = BudidayaAdapter {
            startActivity(LaporanKeuanganActivity.createIntent(
                this,
                ArrayList(mListDataInvestorSpk),
                ArrayList(mListDataBodySpk), it
            ))
            Utility.startTransition(this)
        }
        rv_budidaya.layoutManager = LinearLayoutManager(this)
        rv_budidaya.adapter = budidayaAdapter

        spinner_spk.apply {
            setSpinnerAdapter(IconSpinnerAdapter(this))
            //lifecycleOwner = this@MonitoringBudidayaActivity
        }

        if (investorId != null && role != null) {
            onStartProgress()
            presenter.getInvestorSpk(investorId.toString(), role)
        }
    }

    override fun onStartProgress() {
        tv_error.visibility = View.GONE
        progress_bar.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onSuccessGetInvestorSpk(listDataInvestorSpk: List<DataInvestorSpk>) {
        onStopProgress()
        mListDataInvestorSpk.clear()
        mListDataInvestorSpk.addAll(listDataInvestorSpk)
        if (listDataInvestorSpk.isNotEmpty()) {

            val listItemSpinner = mutableListOf<IconSpinnerItem>()
            for (spk in listDataInvestorSpk) {
                val nominal: Int? = spk.nominal?.toIntOrNull()
                listItemSpinner.add(
                    IconSpinnerItem(null, "${spk.nomor_spk}\n${if (nominal != null) Utility.setFormatRupiah(nominal) else spk.nominal}")
                )
            }

            spinner_spk.setItems(ArrayList(listItemSpinner))
            spinner_spk.setOnSpinnerItemSelectedListener<IconSpinnerItem> { position, item ->
                budidayaAdapter.clearData()
                currentSpkPosition = position
                onStartProgress()
                presenter.getBodySpk(listDataInvestorSpk[position].spk_id!!)
            }

        }else {

        }
    }

    override fun onSuccessGetBodySpk(listDataBodySpk: List<DataBodySpk>) {
        onStopProgress()
        mListDataBodySpk.clear()
        mListDataBodySpk.addAll(listDataBodySpk)
        budidayaAdapter.setData(listDataBodySpk)
    }

    override fun onFailedGetInvestorSpk(message: String) {
        onStopProgress()
        tv_error.setOnClickListener {
            val prefs = PreferencesManager.init(this)
            val investorId = prefs.memberData?.user_id_qta1
            val role = prefs.memberData?.role
            if (investorId != null && role != null) {
                onStartProgress()
                presenter.getInvestorSpk(investorId.toString(), role)
            }
        }
        tv_error.text = "$message\nTap untuk ulangi."
        tv_error.visibility = View.VISIBLE
    }

    override fun onFailedGetBodySpk(message: String) {
        onStopProgress()
        tv_error.setOnClickListener {
            onStartProgress()
            presenter.getBodySpk(mListDataInvestorSpk[currentSpkPosition].spk_id!!)
        }
        tv_error.text = "$message\nTap untuk ulangi."
        tv_error.visibility = View.VISIBLE
    }
}
