package com.kreasikode.qta1.ui.monitoring.budidaya

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class MonitoringBudidayaPresenter(var context: Context, var mainView: MonitoringBudidayaView.MainView): MonitoringBudidayaView.MainPresenter {

    override fun getInvestorSpk(investor_id: String, role_id: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_INVESTOR_SPK)
                .addBodyParameter("investor_id", investor_id)
                .addBodyParameter("role_id", role_id)
                .build().getAsObject(
                    GetInvestorSpkResponse::class.java,
                    object : ParsedRequestListener<GetInvestorSpkResponse> {
                        override fun onResponse(response: GetInvestorSpkResponse) {
                            if (response.data != null) {
                                mainView.onSuccessGetInvestorSpk(response.data)
                            }else {
                                mainView.onFailedGetInvestorSpk("Gagal mengambil data yang diminta.")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedGetInvestorSpk(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onFailedGetInvestorSpk("Koneksi internet offline.")
        }
    }

    override fun getBodySpk(spk_id: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_BODY_SPK)
                .addBodyParameter("spk_id", spk_id)
                .build().getAsObject(
                    GetBodySpkResponse::class.java,
                    object : ParsedRequestListener<GetBodySpkResponse> {
                        override fun onResponse(response: GetBodySpkResponse) {
                            if (response.data != null) {
                                mainView.onSuccessGetBodySpk(response.data)
                            }else {
                                mainView.onFailedGetBodySpk("Gagal mengambil data yang diminta.")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedGetBodySpk(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onFailedGetBodySpk("Koneksi internet offline.")
        }
    }
}