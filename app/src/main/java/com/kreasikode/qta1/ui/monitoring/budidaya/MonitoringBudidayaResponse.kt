package com.kreasikode.qta1.ui.monitoring.budidaya

import com.kreasikode.qta1.model.MonitoringBudidaya

data class MonitoringBudidayaResponse(
    val data: List<MonitoringBudidaya>?
)