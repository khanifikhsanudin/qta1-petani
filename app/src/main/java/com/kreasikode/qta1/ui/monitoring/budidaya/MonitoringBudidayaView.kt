package com.kreasikode.qta1.ui.monitoring.budidaya

import com.kreasikode.qta1.model.DataBodySpk
import com.kreasikode.qta1.model.DataInvestorSpk

class MonitoringBudidayaView {

    interface MainPresenter {
        fun getInvestorSpk(investor_id: String, role_id: String)
        fun getBodySpk(spk_id: String)
    }

    interface MainView {

        fun onStartProgress()
        fun onStopProgress()
        fun onSuccessGetInvestorSpk(listDataInvestorSpk: List<DataInvestorSpk>)
        fun onSuccessGetBodySpk(listDataBodySpk: List<DataBodySpk>)
        fun onFailedGetInvestorSpk(message: String)
        fun onFailedGetBodySpk(message: String)
    }

}