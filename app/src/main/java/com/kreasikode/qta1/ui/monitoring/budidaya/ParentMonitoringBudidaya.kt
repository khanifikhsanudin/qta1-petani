package com.kreasikode.qta1.ui.monitoring.budidaya

import android.os.Parcelable
import com.kreasikode.qta1.model.MonitoringBudidaya
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParentMonitoringBudidaya(
    val nomor_spk: String?,
    val nomor_budidaya: String?,
    val nomor_reg_budidaya: String?,
    val total_nominal: Int?,
    val list: List<MonitoringBudidaya>
) : Parcelable