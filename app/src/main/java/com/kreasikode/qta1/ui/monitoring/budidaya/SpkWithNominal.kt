package com.kreasikode.qta1.ui.monitoring.budidaya

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SpkWithNominal(
    val nomor_spk: String,
    val nominal_spk: Int
) : Parcelable