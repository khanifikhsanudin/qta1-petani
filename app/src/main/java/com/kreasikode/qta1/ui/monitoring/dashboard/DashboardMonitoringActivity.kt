package com.kreasikode.qta1.ui.monitoring.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.kreasikode.qta1.R
import com.kreasikode.qta1.model.DataInvestorOverview
import com.kreasikode.qta1.ui.monitoring.budidaya.MonitoringBudidayaActivity
import com.kreasikode.qta1.utils.PreferencesManager
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_dashboard_monitoring.*

class DashboardMonitoringActivity : AppCompatActivity(), DashboardMonitoringView.MainView {

    private lateinit var presenter: DashboardMonitoringPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_monitoring)
        presenter = DashboardMonitoringPresenter(this, this)
        setupView()
    }

    private fun setupView() {

        val prefs = PreferencesManager.init(this)
        val investorId = prefs.memberData?.user_id_qta1
        val role = prefs.memberData?.role

        navigation_back.setOnClickListener {
            finish()
        }

        menu_jumlah_spk.setOnClickListener {
            startActivity(Intent(this, MonitoringBudidayaActivity::class.java))
            Utility.startTransition(this)
        }

        swipe_refresh.setOnRefreshListener {
            if (investorId != null && role != null) {
                presenter.getInvestorOverview(investorId.toString(), role)
            }
        }

        if (investorId != null && role != null) {
            presenter.getInvestorOverview(investorId.toString(), role)
        }
    }

    override fun onStartProgress() {
        tv_error.visibility = View.GONE
        swipe_refresh.isRefreshing = true
    }

    override fun onStopProgress() {
        swipe_refresh.isRefreshing = false
    }

    @SuppressLint("SetTextI18n")
    override fun onSuccessGetInvestorOverview(listDataInvestorOverview: List<DataInvestorOverview>) {
        onStopProgress()
        ll_menu.visibility = View.VISIBLE
        val data = listDataInvestorOverview[0]
        tv_jumlah_spk.text = data.jml_spk
        tv_total_investasi.text = Utility.setFormatRupiah(data.spk_nominal_total?.toIntOrNull()?:0)
        tv_penyaluran_dana.text = Utility.setFormatRupiah(data.spk_nominal_terpakai?.toIntOrNull()?:0)
        tv_jumlah_petani.text = "${data.jml_mitra?:0} Orang"
        tv_jumlah_lahan.text = data.jml_lahan?:"0"
    }

    @SuppressLint("SetTextI18n")
    override fun onFailed(message: String) {
        onStopProgress()
        tv_error.text = "${getString(R.string.refreshing_text)}\n Error: $message."
        tv_error.visibility = View.VISIBLE
    }

    @SuppressLint("SetTextI18n")
    override fun onOffline() {
        onStopProgress()
        tv_error.text = "${getString(R.string.refreshing_text)}\n Error: Tidak ada koneksi internet."
        tv_error.visibility = View.VISIBLE
    }
}
