package com.kreasikode.qta1.ui.monitoring.dashboard

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class DashboardMonitoringPresenter(
    private val context: Context,
    private val mainView: DashboardMonitoringView.MainView
): DashboardMonitoringView.MainPresenter {

    override fun getInvestorOverview(investor_id: String, role_id: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_INVESTOR_OVERVIEW)
                .addBodyParameter("investor_id", investor_id)
                .addBodyParameter("role_id", role_id)
                .build().getAsObject(
                    DashboardMonitoringResponse::class.java,
                    object : ParsedRequestListener<DashboardMonitoringResponse> {
                        override fun onResponse(response: DashboardMonitoringResponse) {
                            if (response.data != null) {
                                mainView.onSuccessGetInvestorOverview(response.data)
                            }else {
                                mainView.onFailed("Gagal mengambil data yang diminta.")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailed(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}