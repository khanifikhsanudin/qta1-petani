package com.kreasikode.qta1.ui.monitoring.dashboard

import com.kreasikode.qta1.model.DataInvestorOverview

data class DashboardMonitoringResponse(
    val data: List<DataInvestorOverview>?
)