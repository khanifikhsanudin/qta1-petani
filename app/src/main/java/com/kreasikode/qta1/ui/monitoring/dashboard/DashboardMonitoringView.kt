package com.kreasikode.qta1.ui.monitoring.dashboard

import com.kreasikode.qta1.model.DataInvestorOverview

class DashboardMonitoringView {

    interface MainPresenter {
        fun getInvestorOverview(investor_id: String, role_id: String)
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onSuccessGetInvestorOverview(listDataInvestorOverview: List<DataInvestorOverview>)
        fun onFailed(message: String)
        fun onOffline()
    }
}