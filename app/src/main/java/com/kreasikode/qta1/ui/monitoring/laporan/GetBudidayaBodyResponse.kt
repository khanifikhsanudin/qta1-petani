package com.kreasikode.qta1.ui.monitoring.laporan

import com.kreasikode.qta1.model.DataBudidayaBody

data class GetBudidayaBodyResponse(
    val data: List<DataBudidayaBody>?
)