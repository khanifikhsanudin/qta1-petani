package com.kreasikode.qta1.ui.monitoring.laporan

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.LaporanKeuanganProsesAdapter
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.global.Extras
import com.kreasikode.qta1.model.DataBodySpk
import com.kreasikode.qta1.model.DataBudidayaBody
import com.kreasikode.qta1.model.DataInvestorSpk
import com.kreasikode.qta1.utils.ImageUtil
import com.kreasikode.qta1.utils.Utility
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.activity_laporan_keuangan.*

class LaporanKeuanganActivity : AppCompatActivity(), LaporanKeuanganView.MainView {

    private val mListDataInvestorSpk: List<DataInvestorSpk>
            by lazy { intent.getParcelableArrayListExtra<DataInvestorSpk>(
                Extras.EXTRA_LIST_DATA_INVESTOR_SPK
            )?.toList()?: emptyList() }

    private lateinit var mListDataBodySpk: List<DataBodySpk>

    private var mCurrentDataBodySpk: DataBodySpk? = null

    private lateinit var presenter: LaporanKeuanganPresenter

    private lateinit var laporanKeuanganProsesAdapter: LaporanKeuanganProsesAdapter

    private var currentSpk: String = ""
    private var currentBudidaya: String = ""

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_laporan_keuangan)
        presenter = LaporanKeuanganPresenter(this, this)

        navigation_back.setOnClickListener { finish() ; Utility.finishTransition(this) }

        mListDataBodySpk = intent.getParcelableArrayListExtra<DataBodySpk>(
            Extras.EXTRA_LIST_DATA_BODY_SPK
        )?.toList()?: emptyList()

        mCurrentDataBodySpk = intent.getParcelableExtra<DataBodySpk>(
            Extras.EXTRA_CURRENT_DATA_BODY_SPK
        )?: DataBodySpk()

        laporanKeuanganProsesAdapter = LaporanKeuanganProsesAdapter { monitoring, ivProcess ->
            val listPosterImage = mutableListOf<String>()

            if (!monitoring.gambar_1.isNullOrEmpty()) {
                listPosterImage.add(EndPoints.BASE_URL_IMAGE_NEW.plus(monitoring.gambar_1))
            }

            if (!monitoring.gambar_2.isNullOrEmpty()) {
                listPosterImage.add(EndPoints.BASE_URL_IMAGE_NEW.plus(monitoring.gambar_2))
            }

            StfalconImageViewer.Builder<String>(this, listPosterImage) { view, image ->
                view.setBackgroundResource(R.color.black)
                ImageUtil.fromUrl(this, image, view, placeholder = false)
            }.withHiddenStatusBar(false).withTransitionFrom(ivProcess).withOverlayView(
                LayoutInflater.from(this).inflate(R.layout.caption_images,null, false)
                    .apply {
                        findViewById<TextView>(R.id.tv_caption).text = "${monitoring.proses} ${monitoring.reg_budidaya}"
                    }
            ).show(false)
        }
        rv_process.layoutManager = LinearLayoutManager(this)
        rv_process.adapter = laporanKeuanganProsesAdapter

        val listItemSpinner = mutableListOf<IconSpinnerItem>()
        for (spk in mListDataInvestorSpk) {
            listItemSpinner.add(
                IconSpinnerItem(null, "${spk.nomor_spk}\n${Utility.setFormatRupiah(spk.nominal?.toIntOrNull()?:0)}")
            )
        }

        spinner_spk.apply {
            setSpinnerAdapter(IconSpinnerAdapter(this))
            setItems(ArrayList(listItemSpinner))
            //lifecycleOwner = this@LaporanKeuanganActivity
        }

        spinner_budidaya.apply {
            setSpinnerAdapter(IconSpinnerAdapter(this))
            //lifecycleOwner = this@LaporanKeuanganActivity
        }

        spinner_spk.setOnSpinnerItemSelectedListener<IconSpinnerItem> { position, item ->
            val nomorSpk = mListDataInvestorSpk.map { it.spk_id!! }[position]
            if (nomorSpk != currentSpk) {
                currentSpk = nomorSpk
                val listItemSpinnerBuidaya = mutableListOf<IconSpinnerItem>()
                if (mCurrentDataBodySpk?.spk_id == currentSpk) {
                    for (data in mListDataBodySpk) {
                        listItemSpinnerBuidaya.add(
                            IconSpinnerItem(null, "${data.reg_budidaya} (${data.nomor_kontrak})" +
                                    "\n${Utility.setFormatRupiah(data.nom_investasi_budidaya?.toIntOrNull()?:0)}")
                        )
                    }
                    spinner_budidaya.clearSelectedItem()
                    spinner_budidaya.setItems(ArrayList(listItemSpinnerBuidaya))
                }else {
                    currentSpk = nomorSpk
                    tabel_info.visibility = View.GONE
                    laporanKeuanganProsesAdapter.clearData()
                    onStartProgress()
                    presenter.getBodySpk(currentSpk)
                }
            }
        }

        spinner_budidaya.setOnSpinnerItemSelectedListener<IconSpinnerItem> { position, item ->
            if (mListDataBodySpk[position].budidaya_id != currentBudidaya) {
                setupViewBudidaya(mListDataBodySpk[position])
                currentBudidaya = mListDataBodySpk[position].budidaya_id?:""
            }
        }

        mCurrentDataBodySpk?.let {
            setupView(it)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupView(dataBodySpk: DataBodySpk) {
        if(dataBodySpk.spk_id != null) {
            spinner_spk.selectItemByIndex(mListDataInvestorSpk.indexOfFirst { it.spk_id == dataBodySpk.spk_id })
            spinner_budidaya.selectItemByIndex(mListDataBodySpk.indexOfFirst { it.budidaya_id == mCurrentDataBodySpk?.budidaya_id })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupViewBudidaya(dataBodySpk: DataBodySpk) {
        tabel_info.visibility = View.VISIBLE
        tv_nomor.text = "${dataBodySpk.reg_budidaya} (${dataBodySpk.nomor_kontrak})"
        tv_funding.text = Utility.setFormatRupiah(dataBodySpk.nom_investasi_budidaya?.toIntOrNull()?:0)
        tv_jumlah_pemakaian.text = Utility.setFormatRupiah(dataBodySpk.serapan?.toIntOrNull()?:0)
        tv_saldo.text = Utility.setFormatRupiah(dataBodySpk.sisa_per_budidaya?.toIntOrNull()?:0)
        tv_mitra.text = dataBodySpk.mitra
        tv_pembina.text = dataBodySpk.pembina
        tv_alamat.text = "${Utility.capitalizeWords(dataBodySpk.provinsi?:"")} ${Utility.capitalizeWords(dataBodySpk.kabupaten?:"")} ${Utility.capitalizeWords(dataBodySpk.kecamatan?:"")} ${Utility.capitalizeWords(dataBodySpk.kelurahan?:"")}"
        onStartProgress()
        presenter.getBudidayaBody(dataBodySpk.budidaya_id?:"")
    }

    override fun onStartProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onSuccessGetBodySpk(listDataBodySpk: List<DataBodySpk>) {
        onStopProgress()
        mListDataBodySpk = listDataBodySpk
        mCurrentDataBodySpk = null
        val listItemSpinnerBuidaya = mutableListOf<IconSpinnerItem>()
        for (data in mListDataBodySpk) {
            listItemSpinnerBuidaya.add(
                IconSpinnerItem(null, "${data.reg_budidaya} (${data.nomor_kontrak})" +
                        "\n${Utility.setFormatRupiah(data.nom_investasi_budidaya?.toIntOrNull()?:0)}")
            )
        }
        spinner_budidaya.clearSelectedItem()
        spinner_budidaya.setItems(ArrayList(listItemSpinnerBuidaya))
    }

    override fun onSuccessGetBudidayaBody(listDataBudidayaBody: List<DataBudidayaBody>) {
        onStopProgress()
        laporanKeuanganProsesAdapter.setData(listDataBudidayaBody)
    }

    override fun onFailedGetBodySpk(message: String) {
        onStopProgress()
        tv_error.setOnClickListener {
            onStartProgress()
            presenter.getBodySpk(currentSpk)
        }
        tv_error.text = "$message\nTap untuk ulangi."
        tv_error.visibility = View.VISIBLE

    }

    override fun onFailedGetBudidayaBody(message: String) {
        onStopProgress()
        tv_error.setOnClickListener {
            onStartProgress()
            presenter.getBudidayaBody(currentBudidaya)
        }
        tv_error.text = "$message\nTap untuk ulangi."
        tv_error.visibility = View.VISIBLE
    }

    companion object {

        fun createIntent(
            context: Context,
            listDataInvestorSpk: ArrayList<DataInvestorSpk>,
            listDataBodySpk: ArrayList<DataBodySpk>,
            currentDataBodySpk: DataBodySpk
        ): Intent {
            val intent = Intent(context, LaporanKeuanganActivity::class.java)
            intent.putExtra(Extras.EXTRA_LIST_DATA_INVESTOR_SPK, listDataInvestorSpk)
            intent.putExtra(Extras.EXTRA_LIST_DATA_BODY_SPK, listDataBodySpk)
            intent.putExtra(Extras.EXTRA_CURRENT_DATA_BODY_SPK, currentDataBodySpk)
            return intent
        }
    }
}
