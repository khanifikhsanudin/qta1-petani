package com.kreasikode.qta1.ui.monitoring.laporan

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.ui.monitoring.budidaya.GetBodySpkResponse
import com.kreasikode.qta1.utils.NetworkUtil

class LaporanKeuanganPresenter(
    private val context: Context,
    private val mainView: LaporanKeuanganView.MainView
): LaporanKeuanganView.MainPresenter {
    override fun getBodySpk(spk_id: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_BODY_SPK)
                .addBodyParameter("spk_id", spk_id)
                .build().getAsObject(
                    GetBodySpkResponse::class.java,
                    object : ParsedRequestListener<GetBodySpkResponse> {
                        override fun onResponse(response: GetBodySpkResponse) {
                            if (response.data != null) {
                                mainView.onSuccessGetBodySpk(response.data)
                            }else {
                                mainView.onFailedGetBodySpk("Gagal mengambil data yang diminta.")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedGetBodySpk(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onFailedGetBodySpk("Koneksi internet offline.")
        }
    }

    override fun getBudidayaBody(budidaya_id: String) {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_BUDIDAYA_BODY)
                .addBodyParameter("budidaya_id", budidaya_id)
                .build().getAsObject(
                    GetBudidayaBodyResponse::class.java,
                    object : ParsedRequestListener<GetBudidayaBodyResponse> {
                        override fun onResponse(response: GetBudidayaBodyResponse) {
                            if (response.data != null) {
                                mainView.onSuccessGetBudidayaBody(response.data)
                            }else {
                                mainView.onFailedGetBudidayaBody("Gagal mengambil data yang diminta.")
                            }
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedGetBudidayaBody(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onFailedGetBudidayaBody("Koneksi internet offline.")
        }
    }

}