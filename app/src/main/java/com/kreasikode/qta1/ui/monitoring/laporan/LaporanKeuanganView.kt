package com.kreasikode.qta1.ui.monitoring.laporan

import com.kreasikode.qta1.model.DataBodySpk
import com.kreasikode.qta1.model.DataBudidayaBody

class LaporanKeuanganView {

    interface MainPresenter {
        fun getBodySpk(spk_id: String)
        fun getBudidayaBody(budidaya_id: String)
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onSuccessGetBodySpk(listDataBodySpk: List<DataBodySpk>)
        fun onSuccessGetBudidayaBody(listDataBudidayaBody: List<DataBudidayaBody>)
        fun onFailedGetBodySpk(message: String)
        fun onFailedGetBudidayaBody(message: String)
    }
}