package com.kreasikode.qta1.ui.order

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.OrderAdapter
import com.kreasikode.qta1.adapter.TasAdapter
import com.kreasikode.qta1.global.DataConst
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.ui.toko.TokoActivity
import com.skydoves.powerspinner.createPowerSpinnerView
import kotlinx.android.synthetic.main.activity_detail_order.*
import kotlinx.android.synthetic.main.activity_order_product.*

class DetailOrderActivity : AppCompatActivity() {
    private lateinit var itemAdapter:OrderAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_order)
        navigation_back_detail.setOnClickListener { finish() }

        if (DataConst.DataKonsumen.id==""){
            Toast.makeText(applicationContext,"Data Form Kosong",Toast.LENGTH_SHORT).show()
        }
        tv_name_order_detail.text = DataConst.DataKonsumen.name
        tv_address_detail.text = DataConst.DataKonsumen.alamat+", "+DataConst.DataKonsumen.kecamatan+", "+DataConst.DataKonsumen.kota+", "+DataConst.DataKonsumen.provinsi
        tv_email_detail.text = DataConst.DataKonsumen.email
        tv_notelp_detail.text = DataConst.DataKonsumen.notelp

        itemAdapter = OrderAdapter()
        itemAdapter.setData(DataConst.DataOrder)
        rv_order_detail.layoutManager = LinearLayoutManager(this)
        rv_order_detail.adapter = itemAdapter

        var total = 0
        for (item: DataProduk in itemAdapter.getData()){
            if (rv_order_detail.adapter != null){
                itemAdapter.notifyDataSetChanged()
                val harga = item.harga_ecer!!.toInt() * item.qty!!.toInt()
                total = total + harga
            }
        }

        tv_harga_total_detail_order.text = total.toString().trim()

        btn_edit_detail_order.setOnClickListener {
            val intent = Intent(this,OrderProductActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn_lanjut_detail_order.setOnClickListener {
            val intent = Intent(this,TokoActivity::class.java)
            startActivity(intent)
            DataConst.DataOrder.clear()
            Toast.makeText(applicationContext,"Order Dikirimkan",Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}