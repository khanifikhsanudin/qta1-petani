package com.kreasikode.qta1.ui.order

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kreasikode.qta1.R
import com.kreasikode.qta1.global.DataConst
import com.kreasikode.qta1.model.DataPerson
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_form_order.*

class FormOrderActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_order)

        navigation_back_Form.setOnClickListener { finish() }
        ed_kode_telp.isEnabled = false

        if (DataConst.DataKonsumen.id!=""){
            ed_nama_order.setText(DataConst.DataKonsumen.name)
            ed_email.setText(DataConst.DataKonsumen.email)
            ed_alamat_order.setText(DataConst.DataKonsumen.alamat)
            ed_no_telp.setText(DataConst.DataKonsumen.notelp)
            ed_kecamatan.setText(DataConst.DataKonsumen.kecamatan)
            ed_kota.setText(DataConst.DataKonsumen.kota)
            ed_provinsi.setText(DataConst.DataKonsumen.provinsi)
        }
        

        btn_lanjut_form_order.setOnClickListener {
            val nama = ed_nama_order.text.toString().trim()
            val emailk = ed_email.text.toString().trim()
            val alamat = ed_alamat_order.text.toString().trim()
            val notelp = ed_no_telp.text.toString().trim()
            val kodetelp = ed_kode_telp.text.toString().trim()
            val kecamatan = ed_kecamatan.text.toString().trim()
            val kota = ed_kota.text.toString().trim()
            val prov = ed_provinsi.text.toString().trim()

            if (nama.equals("")){
                ed_nama_order.error = "Harap diiisi"
            }else if (!Utility.isEmailValid(emailk)){
                ed_email.error = "harap Diisi"
            }else if (alamat.equals("")){
                ed_alamat_order.error = "harap Diisi"
            }else if (notelp.equals("")){
                ed_no_telp.error = "harap Diisi"
            }else if (kecamatan.equals("")){
                ed_kecamatan.error = "harap Diisi"
            }else if (kota.equals("")){
                ed_kota.error = "harap Diisi"
            }else if (prov.equals("")){
                ed_provinsi.error = "harap Diisi"
            }

            val tempForm = DataPerson("1",nama,alamat,kodetelp.plus(notelp),emailk,kecamatan,kota,prov)
            DataConst.DataKonsumen = tempForm
            val intent = Intent(this,DetailOrderActivity::class.java)
            startActivity(intent)
        }

    }
}