package com.kreasikode.qta1.ui.order

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.TasAdapter
import com.kreasikode.qta1.global.DataConst
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_order_product.*

class OrderProductActivity : AppCompatActivity(),TasAdapter.OnClick {
    private lateinit var tasAdapter: TasAdapter
    private var orderProductBefore = emptyList<DataProduk>()
    private var total = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_product)
        navigation_back_order.setOnClickListener { finish() }
        tasAdapter = TasAdapter(DataConst.DataOrder, this)
        tasAdapter.setData(DataConst.DataOrder)
        navigation_back_order.setOnClickListener { finish() }
        rv_order_produk.layoutManager = LinearLayoutManager(this)
        rv_order_produk.adapter = tasAdapter

        btn_lanjut_order.setOnClickListener {
            if(DataConst.DataOrder.isNullOrEmpty()){
                Toast.makeText(applicationContext,"Anda belum memasukkan barang ke keranjang",Toast.LENGTH_SHORT).show()
            }else{
                val intent = Intent(this,FormOrderActivity::class.java)
                startActivity(intent)
            }
        }

        total()
    }

    override fun clickPlus() {
        total()
    }

    override fun clickMinus() {
        total()
    }

    fun total(){
        tasAdapter.setData(DataConst.DataOrder)
        for (item: DataProduk in tasAdapter.getData()){
            if (rv_order_produk.adapter != null){
                tasAdapter.notifyDataSetChanged()
                var harga = item.harga_ecer!!.toInt() * item.qty!!.toInt()
                total = total + harga
            }
        }

        tv_total_order_product.text = Utility.setFormatRupiah(tasAdapter.getTotal())
        tasAdapter.notifyDataSetChanged()
        rv_order_produk.adapter = tasAdapter
    }

}