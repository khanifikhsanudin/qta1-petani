package com.kreasikode.qta1.ui.tentang

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.kreasikode.qta1.R
import com.kreasikode.qta1.utils.ImageUtil
import kotlinx.android.synthetic.main.activity_tentang.*

class TentangActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tentang)

        navigation_back.setOnClickListener { finish() }

        ImageUtil.fromDrawable(this.applicationContext, ContextCompat.getDrawable(this, R.drawable.qty_logo)!!, iv_logo)
    }
}
