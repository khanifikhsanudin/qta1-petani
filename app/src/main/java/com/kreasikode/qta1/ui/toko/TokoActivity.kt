package com.kreasikode.qta1.ui.toko

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.kreasikode.qta1.R
import com.kreasikode.qta1.adapter.ProductTokoAdapter
import com.kreasikode.qta1.model.DataProduk
import com.kreasikode.qta1.ui.detail.toko.DetailProductActivity
import com.kreasikode.qta1.ui.order.OrderProductActivity
import com.kreasikode.qta1.utils.Utility
import kotlinx.android.synthetic.main.activity_toko.*

class TokoActivity : AppCompatActivity(), TokoView.MainView {

    private lateinit var presenter: TokoPresenter
    private lateinit var productTokoAdapter: ProductTokoAdapter
    private var productTokoBefore = emptyList<DataProduk>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toko)
        presenter = TokoPresenter(this, this)
        navigation_back.setOnClickListener { finish() }
        productTokoAdapter = ProductTokoAdapter { startActivity(DetailProductActivity.createIntent(this, it)) ; Utility.startTransition(this)}
        rv_produk.layoutManager = GridLayoutManager(this, 2)
        rv_produk.adapter = productTokoAdapter
        but_bag.setOnClickListener {
            val intent = Intent(this,OrderProductActivity::class.java)
            startActivity(intent)
        }

//        val listProductToko = mutableListOf<ProductToko>(
//            ProductToko(
//                1,
//                "Rudi Budiman\nYogyakarta",
//                R.drawable.img_beras_merah,
//                "Beras Merah",
//                10000,
//                "800gr",
//                "Beras merupakan makanan pokok bagi sejumlah negara di dunia, terutama di Asia. Ada begitu banyak macam beras. Salah satunya beras merah. Beras merah ialah beras yang hanya mengalami pengelupasan lapisan paling luar dan tidak melalui proses penggilingan."
//            ), ProductToko(
//                1,
//                "Rudi Budiman\nYogyakarta",
//                R.drawable.img_beras_cianjur,
//                "Beras Cianjur",
//                12000,
//                "1000gr",
//                "Beras Menteri (Padi Pandanwangi) Cianjur. Pandanwangi adalah salah satu varietas dari padi bulu yang ditanam di Cisalak, Cibeber, Cianjur, Jawa Barat. Karena nasinya yang beraroma pandan, maka padi dan beras ini sejak tahun 1973 terkenal dengan sebutan Pandanwangi"
//            ), ProductToko(
//                1,
//                "Suparno\nYogyakarta",
//                R.drawable.img_beras_mentik,
//                "Beras Menthik Wangi",
//                10000,
//                "1000gr",
//                " Inspirasi Resep Tips\n" +
//                        "Kelebihan dan Tips Memasak Beras Menthik Wangi yang Tepat, Cocok untuk Beragam Olahan\n" +
//                        "\n" +
//                        "@ abcnoticias.mx\n" +
//                        "\n" +
//                        "Beras merupakan salah satu makanan pokok masyarakat Indonesia. Beras sendiri memiliki jenis yang cukup beragam dengan harga yang tentunya berbeda antara satu dengan yang lainnya. Tinggal Anda pilih sendiri jenis beras mana yang sesuai selera lidah dan pastinya cocok di kantong. Adapun salah satu jenis beras yang bisa dicoba adalah beras menthik wangi."
//            ), ProductToko(
//                1,
//                "Suparno\nYogyakarta",
//                R.drawable.img_beras_cempat,
//                "Beras C4",
//                11000,
//                "900gr",
//                "Beras C4 Premium Produksi Baru terus.. Ready stock\n" +
//                        "\n" +
//                        "Jaminan Enak dan Pulen. \n" +
//                        "\n" +
//                        "Benih Beras IR64\n" +
//                        "Jenis C4\n" +
//                        "Kualitas Beras Premium\n" +
//                        "Derajat Sosoh 100%\n" +
//                        "Kadar Air maks 16%\n" +
//                        "Menir maks 1%\n" +
//                        "Broken maks 10%\n" +
//                        "\n" +
//                        "Dijamin pulen dan enak untuk konsumsi harian masyarakat kelas menengah ke atas.."
//            ), ProductToko(
//                1,
//                "Deva Rizki\nYogyakarta",
//                R.drawable.img_rojolele,
//                "Beras Rojo Lele",
//                10000,
//                "800gr",
//                "Deskripsi BERAS ROJOLELE DELANGGU 20 KG\n" +
//                        "\n" +
//                        "Beras, Pulen, \n" +
//                        "Tanpa Pemutih \n" +
//                        "Tanpa Pengawet \n" +
//                        "Tanpa Pewangi\n" +
//                        "Di Proses Secara Higienis \n" +
//                        "Putih Alami Dan Terjamin Kualitasnya. beres pulen"
//            ), ProductToko(
//                1,
//                "Ajeng Quamila\nYogyakarta",
//                R.drawable.img_beras_hitam,
//                "Beras Hitam",
//                15000,
//                "1200gr",
//                "Beras hitam sebenarnya adalah kumpulan berbagai jenis beras dari spesies Oryza sativa L., termasuk beberapa di antaranya adalah beras ketan. Bulir beras hitam utuh mempertahankan semua sifat alaminya karena beras ini tidak melalui proses pemutihan.\n" +
//                        "\n" +
//                        "Nasi hitam memiliki tekstur lengket dan rasa gurih kacang, sangat cocok untuk dibuat bubur, makanan penutup, kue beras hitam tradisional, roti, dan mie. Beras ini juga memiliki tekstur yang lebih padat dibandingkan dengan beras putih."
//            )
//        )

        ed_cari.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            @SuppressLint("DefaultLocale")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(ed_cari.text.toString().trim() != "") {
                    val listFilterProductToko = mutableListOf<DataProduk>()
                    for (productToko: DataProduk in productTokoBefore) {
                        if(productToko.nama_produk!!.toLowerCase().contains(ed_cari.text.toString().trim().toLowerCase())) {
                            listFilterProductToko.add(productToko)
                        }
                    }
                    if(listFilterProductToko.isNullOrEmpty().not()) {
                        productTokoAdapter.getData().equals(listFilterProductToko).not().let {
                            if(it) {
                                productTokoAdapter.setData(listFilterProductToko)
                            }
                        }
                    }else {

                    }
                }else {
                    productTokoAdapter.setData(productTokoBefore)
                }
            }
        })

        onStopProgress()
        presenter.fetchProduk()
    }

    override fun onStartProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onStopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onProdukFetched(listProduk: List<DataProduk>?) {
        onStopProgress()
        if (listProduk.isNullOrEmpty().not()) {
            productTokoAdapter.setData(listProduk!!)
            productTokoBefore = listProduk
        }else {
            Toast.makeText(this, "Produk tidak tersedia saat ini", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onFailedFetchProduk(message: String?) {
        onStopProgress()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onOffline() {
        onStopProgress()
        Toast.makeText(this, "Koneksi Internet Offline", Toast.LENGTH_SHORT).show()
    }
}
