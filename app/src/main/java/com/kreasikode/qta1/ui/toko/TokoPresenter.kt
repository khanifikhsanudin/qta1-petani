package com.kreasikode.qta1.ui.toko

import android.content.Context
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.kreasikode.qta1.global.EndPoints
import com.kreasikode.qta1.utils.NetworkUtil

class TokoPresenter(val context: Context, val mainView: TokoView.MainView): TokoView.MainPresenter {
    override fun fetchProduk() {
        if (NetworkUtil.instance!!.isNetworkAvailable(context.applicationContext)) {
            mainView.onStartProgress()
            AndroidNetworking.post(EndPoints.COMPANY_VERB)
                .setPriority(Priority.HIGH)
                .addBodyParameter("access_token", EndPoints.ACCESS_TOKEN)
                .addBodyParameter("verb", EndPoints.VERB_MASTER_PRODUK)
                .build().getAsObject(
                    TokoResponse::class.java,
                    object : ParsedRequestListener<TokoResponse> {
                        override fun onResponse(response: TokoResponse) {
                            mainView.onProdukFetched(response.data)
                        }

                        override fun onError(anError: ANError) {
                            mainView.onFailedFetchProduk(anError.errorDetail)
                        }
                    })
        } else {
            mainView.onOffline()
        }
    }
}