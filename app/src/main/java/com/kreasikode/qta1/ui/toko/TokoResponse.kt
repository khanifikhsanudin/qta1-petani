package com.kreasikode.qta1.ui.toko

import com.kreasikode.qta1.model.DataProduk

data class TokoResponse(
    val data: List<DataProduk>?
)