package com.kreasikode.qta1.ui.toko

import com.kreasikode.qta1.model.DataProduk

class TokoView {

    interface MainPresenter {
        fun fetchProduk()
    }

    interface MainView {
        fun onStartProgress()
        fun onStopProgress()
        fun onProdukFetched(listProduk: List<DataProduk>?)
        fun onFailedFetchProduk(message: String?)
        fun onOffline()
    }
}