package com.kreasikode.qta1.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.kreasikode.qta1.R


object ImageUtil {
    fun fromUrl(
        context: Context,
        url: String,
        imageView: ImageView,
        requestsListener: RequestListener<Drawable>? = null,
        placeholder: Boolean = true
    ) {
        val requestOptions =
            RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)
        Glide.with(context)
            .load(url).apply(requestOptions)
            .placeholder(if (placeholder) R.color.gray_light else android.R.color.transparent)
            .listener(requestsListener)
            .into(imageView)
    }


    fun fromDrawable(context: Context, drawable: Drawable, imageView: ImageView) {
        Glide.with(context).load(drawable).placeholder(R.color.gray_light).into(imageView)
    }
}