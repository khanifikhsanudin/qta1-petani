package com.kreasikode.qta1.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.kreasikode.qta1.global.Constants
import com.kreasikode.qta1.model.MasterToken
import com.kreasikode.qta1.model.Member


class PreferencesManager() {

    var memberData: Member?
        get() {
            val data = preferences!!.getString(Constants.PREF_MEMBER_DATA, "")
            return data?.let { Gson().fromJson(data, Member::class.java) }
        }
        set(member) {
            preferences!!.edit(commit = true) {
                putString(Constants.PREF_MEMBER_DATA, Gson().toJson(member))
            }
        }

    var masterToken: MasterToken?
        get() {
            val data = preferences!!.getString(Constants.PREF_MASTER_TOKEN, "")
            return data?.let { Gson().fromJson(data, MasterToken::class.java) }
        }
        set(masterToken) {
            preferences!!.edit(commit = true) {
                putString(Constants.PREF_MASTER_TOKEN, Gson().toJson(masterToken))
            }
        }

    companion object {
        var preferences: SharedPreferences? = null
            private set

        fun init(context: Context): PreferencesManager {
            preferences = context.getSharedPreferences("this", Context.MODE_PRIVATE)
            return PreferencesManager()
        }
    }
}