package com.kreasikode.qta1.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.util.Patterns
import android.view.*
import android.view.ViewGroup.MarginLayoutParams
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


//mangaid.id | 522: Connection timed out
object Utility {

    private val indoDay = listOf(
        "Minggu" ,
        "Senin"  ,
        "Selasa" ,
        "Rabu"   ,
        "Kamis"  ,
        "Jum'at" ,
        "Sabtu"
    )

    private val indoMoon = listOf(
        "Januari"   ,
        "Februari"  ,
        "Maret"     ,
        "April"     ,
        "Mei"       ,
        "Juni"      ,
        "Juli"      ,
        "Agustus"   ,
        "September" ,
        "Oktober"   ,
        "November"  ,
        "Desember"
    )

    fun indoDateFormat(dateString: String, withMilis: Boolean = true): String {
        var result = dateString
        val format = SimpleDateFormat(if (withMilis) "yyyy-MM-dd HH:mm:ss" else "yyyy-MM-dd", Locale.US)
        val date = format.parse(dateString)
        date?.let {
            val calendar = Calendar.getInstance().apply {
                time = it
            }
            result ="${indoDay[calendar.get(Calendar.DAY_OF_WEEK) - 1]}, " +
                    "${calendar.get(Calendar.DATE)} ${indoMoon[calendar.get(Calendar.MONTH)]} " +
                    "${calendar.get(Calendar.YEAR)}"
        }
        return result
    }

    fun setStatusBarColor(activity: Activity, color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(color)
        }
    }

    fun setLayoutFullScreen(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    fun isEmailValid(Email: String): Boolean {
        return Email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(Email).matches()
    }

    fun View.slideUp(duration: Int = 500) {
        visibility = View.VISIBLE
        val animate = TranslateAnimation(0f, 0f, this.height.toFloat(), 0f)
        animate.duration = duration.toLong()
        animate.fillAfter = true
        this.startAnimation(animate)
    }

    fun View.slideDown(duration: Int = 500) {
        visibility = View.VISIBLE
        val animate = TranslateAnimation(0f, 0f, 0f, this.height.toFloat())
        animate.duration = duration.toLong()
        animate.fillAfter = true
        this.startAnimation(animate)
    }

    fun dpToPx(dp: Int, context: Context): Int {
        val density = context.resources.displayMetrics.density
        return (dp.toFloat() * density).roundToInt()
    }

    val Int.dpToPx: Int
        get() = (this * Resources.getSystem().displayMetrics.density).toInt()

    fun pixelTodp(c: Context, pixel: Float): Float {
        val density = c.resources.displayMetrics.density
        return pixel / density
    }

    fun setMargins(
        view: View,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int
    ) {
        if (view.layoutParams is MarginLayoutParams) {
            val p = view.layoutParams as MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    fun getStatusBarHeight(context: Context): Int {
        var result = 0
        val resourceId: Int = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun setProgressDialog(context: Context, message: String): AlertDialog {
        val ll = LinearLayout(context).apply {
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.gravity = Gravity.CENTER

            val progressBar = ProgressBar(context).apply {
                isIndeterminate = true
                setPadding(0, 0, 40, 0)
                layoutParams = params
            }

            val tvMessage = TextView(context).apply {
                params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                params.gravity = Gravity.CENTER
                text = message
                setTextColor(Color.parseColor("#000000"))
                textSize = 18f
                layoutParams = params
            }

            orientation = LinearLayout.HORIZONTAL
            setPadding(40, 40, 40, 40)
            gravity = Gravity.CENTER
            layoutParams = params

            addView(progressBar)
            addView(tvMessage)
        }

        val builder = AlertDialog.Builder(context).apply {
            setCancelable(false)
            setView(ll)
        }

        return builder.create().apply {
            window?.let {
                val layoutParams = WindowManager.LayoutParams().apply {
                    copyFrom(it.attributes)
                    width = LinearLayout.LayoutParams.WRAP_CONTENT
                    height = LinearLayout.LayoutParams.WRAP_CONTENT
                }
                it.attributes = layoutParams
            }
        }
    }

    fun calculateNoOfColumns(
        context: Context,
        columnWidthDp: Float
    ): Int { // For example columnWidthdp=180
        val displayMetrics = context.resources.displayMetrics
        val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
        return (screenWidthDp / columnWidthDp + 0.5).toInt()
    }

    @SuppressLint("DefaultLocale")
    fun setFormatRupiah(number: Int): String {
        val kursIndonesia = NumberFormat.getCurrencyInstance(Locale("in", "ID"))
        return kursIndonesia.format(number.toDouble()).toLowerCase().replaceFirst("rp", "Rp ")
    }

    @SuppressLint("DefaultLocale")
    fun capitalizeWords(text: String): String {
        return text.split(" ").map{ it.toLowerCase().capitalize() }.joinToString (" ")
    }

    fun startTransition(activity: Activity?) {
        activity?.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    fun finishTransition(activity: Activity?) {
        activity?.finish()
        activity?.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}