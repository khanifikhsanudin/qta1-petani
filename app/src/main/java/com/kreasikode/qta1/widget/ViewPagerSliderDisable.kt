package com.kreasikode.qta1.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class ViewPagerSliderDisable : ViewPager {
    private var _enabled = false

    constructor(context: Context) : super(context) {}
    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        _enabled = false
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return if (_enabled) {
            super.onTouchEvent(ev)
        } else false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return if (_enabled) {
            super.onInterceptTouchEvent(ev)
        } else false
    }

    fun setPagingEnabled(enabled: Boolean) {
        this._enabled = enabled
    }
}